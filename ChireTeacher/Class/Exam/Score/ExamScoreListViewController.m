//
//  ExamScoreListViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/7.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "ExamScoreListViewController.h"

@interface ExamScoreListViewController ()

@end

@implementation ExamScoreListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self sendRequestToGetInfo];
    
}

-(void)sendRequestToGetInfo{
    [[NetworkAdapter sharedAdapter] fetchWithPath:Exam_List requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        
    }];
}

@end
