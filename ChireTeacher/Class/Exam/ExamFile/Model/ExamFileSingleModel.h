//
//  ExamFileSingleModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/17.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ExamFileSingleModel <NSObject>

@end

@interface ExamFileSingleModel : FetchModel

@property(nonatomic,copy)NSString *datetime;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *url;

@end

@interface ExamFileListModel : FetchModel

@property(nonatomic,strong)NSArray<ExamFileSingleModel> *list;

@end


NS_ASSUME_NONNULL_END
