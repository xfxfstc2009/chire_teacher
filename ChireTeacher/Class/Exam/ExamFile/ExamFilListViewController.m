//
//  ExamFilListViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/17.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "ExamFilListViewController.h"
#import "ExamFileSingleModel.h"
#import "ExamScoreListViewController.h"

@interface ExamFilListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *examFileListTableView;
@property (nonatomic,strong)NSMutableArray *examFileMutableArr;
@end

@implementation ExamFilListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetExamFileList];

}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"考试资料";
    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
        ExamScoreListViewController *examScoreVC = [[ExamScoreListViewController alloc]init];
        [self.navigationController pushViewController:examScoreVC animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.examFileMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.examFileListTableView){
        self.examFileListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.examFileListTableView.dataSource = self;
        self.examFileListTableView.delegate = self;
        [self.view addSubview:self.examFileListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.examFileMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    ExamFileSingleModel *examFileModel = [self.examFileMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferCellHeight = 44;
    cellWithRowOne.transferTitle = examFileModel.name;
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ExamFileSingleModel *examFileModel = [self.examFileMutableArr objectAtIndex:indexPath.row];

    PDWebViewController *webViewController = [[PDWebViewController alloc]init];
    NSString *newUrl = [NSString stringWithFormat:@"%@%@",aliyunOSS_BaseURL,examFileModel.url];
    [webViewController webDirectedWebUrl:newUrl];
    [self.navigationController pushViewController:webViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Interface
-(void)sendRequestToGetExamFileList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Exam_File_List requestParams:nil responseObjectClass:[ExamFileListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            ExamFileListModel *examFileListModel = (ExamFileListModel *)responseObject;
            [strongSelf.examFileMutableArr addObjectsFromArray:examFileListModel.list];
            [strongSelf.examFileListTableView reloadData];
        }
    }];
}



@end
