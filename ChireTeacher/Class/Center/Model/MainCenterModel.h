//
//  MainCenterModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/21.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"
#import "StudentModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol MainCenterModel <NSObject>

@end

@interface MainCenterModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *lat;
@property (nonatomic,copy)NSString *lng;
@property (nonatomic,strong)StudentModel *student;

@end

@interface MainCenterListModel : FetchModel

@property (nonatomic,strong)NSArray<MainCenterModel> *list;


@end

NS_ASSUME_NONNULL_END
