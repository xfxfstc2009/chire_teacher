//
//  MainCenterBackViewController.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/21.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AbstractViewController.h"
#import "GWBaseMapViewController.h"
#import "MainCenterMenuViewController.h"
#import "MainCenterModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainCenterBackViewController : GWBaseMapViewController

@property (strong, nonatomic) MainCenterMenuViewController * transferMenuController;

-(void)reloadMapWithList:(MainCenterListModel *)centerListModel;

@end

NS_ASSUME_NONNULL_END
