//
//  MainCenterMenuViewController.h
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2018/1/15.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "AbstractViewController.h"
#define kCenterTopHeaderAvatar LCFloat(80)                      // 头像的宽高
#define kCenterTopHeader LCFloat(100)


@interface MainCenterMenuViewController : AbstractViewController

-(void)activateSwipeToOpenMenu:(BOOL)onlyNavigation;
-(void)openAndCloseMenu;

+(instancetype)sharedAccountModel;

-(void)uploadUserInfo;
-(void)guanzhuManagerStatus:(BOOL)status;
-(void)logoutManager;

@end
