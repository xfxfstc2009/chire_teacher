//
//  MainCenterMenuViewController.m
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2018/1/15.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "MainCenterMenuViewController.h"
#import "MainCenterBackViewController.h"                // 底部的view
#import "MainCenterTopViewController.h"                 // 顶部的view

// Model
#import "MainCenterModel.h"


#define kHeaderHeight 60
@interface MainCenterMenuViewController (){
    CGFloat firstX;
    CGFloat firstY;
    CGPoint _origin;
    CGFloat duration;
}

@property (nonatomic,strong)UIView *backView;               /**< 背部的View*/
@property (nonatomic,strong)UIView *topView;                /**< 顶部的View*/
@property (strong, nonatomic) UIPanGestureRecognizer *panGesture;   /**< 手势*/
@property (nonatomic,strong)MainCenterBackViewController *backController;
@property (nonatomic,strong)MainCenterTopViewController *topController;
@property (nonatomic,strong)UIView *leftView;
@property (nonatomic,strong)UIView *rightView;
@property (nonatomic,assign)BOOL hasLoad;
@end

@implementation MainCenterMenuViewController

+(instancetype)sharedAccountModel{
    static MainCenterMenuViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[MainCenterMenuViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!self.hasLoad){
        self.hasLoad = YES;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createView];
}

#pragma mark - createView
-(void)createView{
    self.view.backgroundColor = [UIColor yellowColor];
    self.backView = [[UIView alloc]init];
    self.backView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - [PDMainTabbarViewController sharedController].tabBarHeight);
    self.backView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.backView.backgroundColor = UURandomColor;
    [self.view addSubview:self.backView];
    
    self.backController = [[MainCenterBackViewController alloc]init];
    self.backController.view.frame = self.view.bounds;
    self.backController.transferMenuController = self;
    [self.backView addSubview:self.backController.view];
    
    self.topView = [[UIView alloc]init];
    self.topView.frame = CGRectMake(0, kCenterTopHeader, kScreenBounds.size.width, self.view.size_height - [PDMainTabbarViewController sharedController].tabBarHeight - kCenterTopHeader);
    //    self.topView.clipsToBounds = YES;
    [self.view addSubview:self.topView];
    
    self.topController = [[MainCenterTopViewController alloc]init];
    self.topController.transferMenuController = self;
    self.topController.view.frame = self.topView.bounds;
    [self.topView addSubview:self.topController.view];
    [self addChildViewController:self.topController];
    
    // 右侧按钮
    self.rightView = [[UIView alloc]init];
    self.rightView.backgroundColor = [UIColor whiteColor];
    self.rightView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(22) - LCFloat(40), 64, LCFloat(40), LCFloat(40));
    self.rightView.layer.cornerRadius = self.rightView.size_width / 2.;
    //    self.rightView.clipsToBounds = YES;
    self.rightView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.rightView.layer.shadowOffset = CGSizeMake(2, 2);
    self.rightView.layer.shadowOpacity = 2.f;
    [self.view addSubview:self.rightView];
    
    UIView *rightViewTop = [[UIView alloc]init];
    rightViewTop.frame = self.rightView.bounds;
    rightViewTop.clipsToBounds = YES;
    rightViewTop.layer.cornerRadius = rightViewTop.size_width / 2.;
    [self.rightView addSubview:rightViewTop];
    
    LOTAnimationView *animation = [LOTAnimationView animationNamed:@"setting"];
    animation.contentMode = UIViewContentModeScaleAspectFill;
    animation.frame = rightViewTop.bounds;
    [rightViewTop addSubview:animation];
    animation.loopAnimation = YES;
    [animation playWithCompletion:NULL];
    
    // tap
    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    actionButton.frame = self.rightView.bounds;
    __weak typeof(self)weakSelf = self;
    [actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;

    }];
    [self.rightView addSubview:actionButton];
    
    // 2. 左侧按钮
    self.leftView = [[UIView alloc]init];
    self.leftView.backgroundColor = [UIColor whiteColor];
    self.leftView.frame = CGRectMake(LCFloat(22), 64, LCFloat(40), LCFloat(40));
    self.leftView.layer.cornerRadius = self.leftView.size_width / 2.;
    self.leftView.layer.shadowColor = [UIColor grayColor].CGColor;
    self.leftView.layer.shadowOffset = CGSizeMake(2, 2);
    self.leftView.layer.shadowOpacity = 2.f;
    [self.view addSubview:self.leftView];
    
    UIView *leftViewTop = [[UIView alloc]init];
    leftViewTop.frame = self.leftView.bounds;
    leftViewTop.clipsToBounds = YES;
    leftViewTop.layer.cornerRadius = leftViewTop.size_width / 2.;
    [self.leftView addSubview:leftViewTop];
    
    LOTAnimationView *animation1 = [LOTAnimationView animationNamed:@"setting"];
    animation1.contentMode = UIViewContentModeScaleAspectFill;
    animation1.animationProgress = 5.;
    animation1.frame = leftViewTop.bounds;
    [leftViewTop addSubview:animation1];
    animation1.loopAnimation = YES;
    [animation1 playWithCompletion:NULL];
    
    // tap
    UIButton *actionButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    actionButton1.frame = self.leftView.bounds;
    [actionButton1 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf directToLive];
    }];
    [self.leftView addSubview:actionButton1];
    
    
    //    if ([AccountModel sharedAccountModel].isShenhe){
    //        self.leftView.hidden = YES;
    //        self.rightView.hidden = NO;
    //    } else {
    self.leftView.hidden = NO;
    self.rightView.hidden = YES;
    //    }
}


-(void)activateSwipeToOpenMenu:(BOOL)onlyNavigation{
    if (!self.panGesture){
        self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPan:)];
    }
    [self.topView addGestureRecognizer:self.panGesture];
}

#pragma mark - 关闭
-(void)openAndCloseMenu{
    CGPoint finalOrigin;
    CGRect f = self.topView.frame;
    
    if (f.origin.y == kCenterTopHeader){
        finalOrigin.y = CGRectGetHeight(self.view.bounds) - kCenterTopHeader - LCFloat(15);
        self.topController.hasEnable = NO;
    } else {
        finalOrigin.y = kCenterTopHeader;
        self.topController.hasEnable = YES;
    }
    finalOrigin.x = 0;
    f.origin = finalOrigin;
    [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.topView.transform = CGAffineTransformIdentity;
        self.topView.frame = f;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)onPan:(UIPanGestureRecognizer *)pan {
    CGPoint translation = [pan translationInView:self.view];
    CGPoint velocity = [pan velocityInView:self.view];
    
    switch (pan.state) {
        case UIGestureRecognizerStateBegan:
            _origin = self.view.frame.origin;
            break;
        case UIGestureRecognizerStateChanged:
            if (_origin.y + translation.y >= 0){
                self.topView.transform = CGAffineTransformMakeTranslation(0, translation.y);
            }
            
            break;
        case UIGestureRecognizerStateEnded:
            
        case UIGestureRecognizerStateCancelled: {
            CGPoint finalOrigin = CGPointZero;
            
            if (velocity.y >= 0) {
                finalOrigin.y = CGRectGetHeight(self.view.bounds) - kCenterTopHeader - LCFloat(15);
                self.topController.hasEnable = NO;
            } else {
                finalOrigin.y = kCenterTopHeader;
                self.topController.hasEnable = YES;
            }
            
            CGRect f = self.topView.frame;
            f.origin = finalOrigin;
            [UIView animateWithDuration:.3 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                self.topView.transform = CGAffineTransformIdentity;
                self.topView.frame = f;
            } completion:^(BOOL finished) {
                
            }];
        }
            break;
            
        default:
            break;
    }
}



#pragma mark - 跳转到直播去直播
-(void)directToLive{
  
}

-(void)uploadUserInfo{
  
}

-(void)guanzhuManagerStatus:(BOOL)status{
  
}

#pragma mark - 退出登录的方法
-(void)logoutManager{

}






@end
