//
//  MainCenterTopViewController.m
//  PPWhaleBaojia
//
//  Created by 裴烨烽 on 2018/1/15.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "MainCenterTopViewController.h"
#import "CenterHeaderTableViewCell.h"
#import "StudentListViewController.h"
#import "ClassRootViewController.h"
#import "MottoListViewController.h"
#import "SchoolListViewController.h"
#import "WorkRootViewController.h"
#import "WechatNoticeListViewController.h"
#import "StudyListViewController.h"
#import "ExamFilListViewController.h"
#import "TeacherRootViewController.h"

@interface MainCenterTopViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *topTableView;
@property (nonatomic,strong)NSArray *topArr;

@end

@implementation MainCenterTopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createSwipeGestore];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)setHasEnable:(BOOL)hasEnable{

}

#pragma mark - 添加手势
-(void)createSwipeGestore{
    [self.transferMenuController activateSwipeToOpenMenu:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = [UIColor whiteColor];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.topArr = @[@[@"头像"],@[@"学生管理",@"班级管理",@"箴言管理",@"学院管理",@"作业管理",@"微信留言管理",@"学习管理",@"测验管理",@"老师管理"],@[@"登录"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.topTableView){
        self.topTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.topTableView.delegate = self;
        self.topTableView.dataSource = self;
        [self.view addSubview:self.topTableView];
        self.topTableView.scrollEnabled = NO;
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.topArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.topArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        CenterHeaderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[CenterHeaderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.backgroundColor = UURandomColor;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录" sourceArr:self.topArr]){
        static NSString *cellIdentifyWithRowLogin = @"cellIdentifyWithRowLogin";
        GWButtonTableViewCell *cellWithRowLogin = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowLogin];
        if (!cellWithRowLogin){
            cellWithRowLogin = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowLogin];
        }
        cellWithRowLogin.transferCellHeight = cellHeight;
        if ([[AccountModel sharedAccountModel] hasLoggedIn]){
            cellWithRowLogin.transferTitle = @"退出登录";
        } else {
            cellWithRowLogin.transferTitle = @"登录";
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowLogin buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            LoginViewController *loginVC = [[LoginViewController alloc]init];
            PDNavigationController *nav = [[PDNavigationController alloc]initWithRootViewController:loginVC];
            [strongSelf.navigationController presentViewController:nav animated:YES completion:NULL];
        }];
        
        return cellWithRowLogin;
    }
    else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = 44;
        cellWithRowTwo.transferTitle = [[self.topArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"学生管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"学生管理" sourceArr:self.topArr]){
        StudentListViewController *studentList = [[StudentListViewController alloc]init];
        [self.navigationController pushViewController:studentList animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"班级管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"班级管理" sourceArr:self.topArr]){
        ClassRootViewController *classRootVC = [[ClassRootViewController alloc]init];
        [self.navigationController pushViewController:classRootVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"箴言管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"箴言管理" sourceArr:self.topArr]){
        MottoListViewController *mottoListVC = [[MottoListViewController alloc]init];
        [self.navigationController pushViewController:mottoListVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"学院管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"学院管理" sourceArr:self.topArr]){
        SchoolListViewController *schoolListVC = [[SchoolListViewController alloc]init];
        [self.navigationController pushViewController:schoolListVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"作业管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"作业管理" sourceArr:self.topArr]){
        WorkRootViewController *workListVC = [[WorkRootViewController alloc]init];
        [self.navigationController pushViewController:workListVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"微信留言管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"微信留言管理" sourceArr:self.topArr]){
        WechatNoticeListViewController *noticeVC = [[WechatNoticeListViewController alloc]init];
        [self.navigationController pushViewController:noticeVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"学习管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"学习管理" sourceArr:self.topArr]){
        StudyListViewController *noticeVC = [[StudyListViewController alloc]init];
        [self.navigationController pushViewController:noticeVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"测验管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"测验管理" sourceArr:self.topArr]){
        ExamFilListViewController *examFileRootVC = [[ExamFilListViewController alloc]init];
        [self.navigationController pushViewController:examFileRootVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"老师管理" sourceArr:self.topArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"老师管理" sourceArr:self.topArr]){
        TeacherRootViewController *teacherVC = [[TeacherRootViewController alloc]init];
        [self.navigationController pushViewController:teacherVC animated:YES];
    }

    
    
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录" sourceArr:self.topArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == 0){
        return [CenterHeaderTableViewCell calculationCellHeight];
    } else {
        return [GWNormalTableViewCell calculationCellHeight];
    }
}

@end
