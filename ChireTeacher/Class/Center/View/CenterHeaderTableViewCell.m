//
//  CenterHeaderTableViewCell.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/21.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "CenterHeaderTableViewCell.h"

@interface CenterHeaderTableViewCell()
@property(nonatomic,strong)UILabel *nameLabel;                  /**< 名字*/
@property (nonatomic,strong)UILabel *descLabel;                 /**< desc*/
@property (nonatomic,strong)UIButton *button;
@property (nonatomic,strong)PDImageView *leftImgView;
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic,strong)NSMutableArray *bottomMutableArr;
@property (nonatomic,strong)UIButton *paihangbangButton;
@property (nonatomic,strong)PDImageView *liwuImgView;
@property (nonatomic,strong)UILabel *liwuLabel;

@end

@implementation CenterHeaderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    
    // 2. 创建名字
    self.nameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.nameLabel];
    
    // 3. 创建desc
    self.descLabel = [GWViewTool createLabelFont:@"提示" textColor:@"黑"];
    [self addSubview:self.descLabel];
    
    // 4. 创建星星
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [self.button buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
    }];
    [self addSubview:self.button];
    
    // 5. 创建desc
    
    self.leftImgView = [[PDImageView alloc]init];
    self.leftImgView.frame = CGRectMake(0 - LCFloat(20), LCFloat(21), LCFloat(40), LCFloat(40));
    self.leftImgView.image = [Tool stretchImageWithName:@"center_header_left_btn"];
    [self addSubview:self.leftImgView];
    
    // 6.底部的view
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.bottomView.frame = CGRectMake(0, self.size_height - LCFloat(50), kScreenBounds.size.width, LCFloat(50));
    [self addSubview:self.bottomView];
    
    self.bottomMutableArr = [NSMutableArray array];
    
    // 7. 创建礼物按钮
    self.liwuImgView = [[PDImageView alloc]init];
    self.liwuImgView.image = [UIImage imageNamed:@"center_header_zhibo_btn"];
    self.liwuImgView.frame = CGRectMake(LCFloat(20) + LCFloat(7), 0, LCFloat(20), LCFloat(20));
    self.liwuImgView.center_y = LCFloat(20);
    [self.leftImgView addSubview:self.liwuImgView];
    
    // 8. 排行榜
    self.liwuLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.liwuLabel.text = @"我要直播";
    CGSize liwuSize = [NSString makeSizeWithLabel:self.liwuLabel];
    self.liwuLabel.frame = CGRectMake(CGRectGetMaxX(self.liwuImgView.frame) + LCFloat(5), 0, liwuSize.width, liwuSize.height);
    self.liwuLabel.center_y = self.liwuImgView.center_y;
    [self.leftImgView addSubview:self.liwuLabel];
    
    self.liwuImgView.alpha = 0;
    self.liwuLabel.alpha = 0;
    
    // 9 .创建按钮
    self.paihangbangButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.paihangbangButton.frame = CGRectMake(0, 0, LCFloat(100), LCFloat(50));
    self.paihangbangButton.center_y = self.liwuImgView.center_y;
    [self.paihangbangButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
//        void(^block)() = objc_getAssociatedObject(strongSelf, &actionCLickWithPaihangbangKey);
//        if (block){
//            block();
//        }
    }];
    [self addSubview:self.paihangbangButton];
}



+(CGFloat)calculationCellHeight{
    return LCFloat(200);
}

@end
