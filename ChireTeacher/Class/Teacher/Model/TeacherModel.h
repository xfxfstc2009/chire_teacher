//
//  TeacherModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TeacherModel <NSObject>

@end

@interface TeacherModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *diploma;
@property (nonatomic,copy)NSString *gender;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *openid;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,copy)NSString *remark;
@property (nonatomic,copy)NSString *school;


@end


@interface TeacherListModel : FetchModel

@property (nonatomic,strong)NSArray<TeacherModel> *list;

@end

NS_ASSUME_NONNULL_END
