//
//  NetworkAdapter+Teacher.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter+Teacher.h"

@implementation NetworkAdapter (Teacher)

// 获取老师列表
-(void)getTeacherListWithBlock:(void(^)(TeacherListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:teacher_list requestParams:nil responseObjectClass:[TeacherListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            TeacherListModel *listModel = (TeacherListModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}


@end
