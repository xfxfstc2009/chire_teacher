//
//  TeacherRootViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "TeacherRootViewController.h"

static char actionClickWithSelectedTeacherBlockKey;
@interface TeacherRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *teacherTableView;
@property (nonatomic,strong)NSMutableArray *teacherMutableArr;
@end

@implementation TeacherRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetTeacherList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"老师管理";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.teacherMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    self.teacherTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
    self.teacherTableView.dataSource = self;
    self.teacherTableView.delegate = self;
    [self.view addSubview:self.teacherTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.teacherMutableArr.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    TeacherModel *teacherModel = [self.teacherMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = teacherModel.name;
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(40);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(self.transferType == TeacherRootViewControllerTypeSelected){
        TeacherModel *teacherModel = [self.teacherMutableArr objectAtIndex:indexPath.row];
        void(^block)(TeacherModel *teacherModel) = objc_getAssociatedObject(self, &actionClickWithSelectedTeacherBlockKey);
        if (block){
            block(teacherModel);
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

#pragma mark - Interface
-(void)sendRequestToGetTeacherList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getTeacherListWithBlock:^(TeacherListModel * _Nonnull listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.teacherMutableArr removeAllObjects];
        [strongSelf.teacherMutableArr addObjectsFromArray:listModel.list];
        [strongSelf.teacherTableView reloadData];
        if (strongSelf.teacherMutableArr.count){
            [strongSelf.teacherTableView dismissPrompt];
        } else{
            [strongSelf.teacherTableView showPrompt:@"当前没有老师信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }
    }];
}

-(void)actionClickWithSelectedTeacherBlock:(void(^)(TeacherModel *teaceherModel))block{
    objc_setAssociatedObject(self, &actionClickWithSelectedTeacherBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
