//
//  TeacherRootViewController.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AbstractViewController.h"
#import "NetworkAdapter+Teacher.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger ,TeacherRootViewControllerType) {
    TeacherRootViewControllerTypeNormal,
    TeacherRootViewControllerTypeSelected,
};

@interface TeacherRootViewController : AbstractViewController

@property (nonatomic,assign)TeacherRootViewControllerType transferType;

-(void)actionClickWithSelectedTeacherBlock:(void(^)(TeacherModel *teaceherModel))block;

@end

NS_ASSUME_NONNULL_END
