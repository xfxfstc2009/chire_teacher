//
//  NetworkAdapter+Teacher.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter.h"
#import "TeacherModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Teacher)

-(void)getTeacherListWithBlock:(void(^)(TeacherListModel *listModel))block;

@end

NS_ASSUME_NONNULL_END
