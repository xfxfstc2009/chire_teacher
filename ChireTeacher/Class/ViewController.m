//
//  ViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "ViewController.h"
#import "ClassRootViewController.h"
#import "MottoListViewController.h"
#import "SchoolListViewController.h"
#import "StudentListViewController.h"
#import "WorkRootViewController.h"
#import "WechatNoticeListViewController.h"
#import "ExamFilListViewController.h"
#import "StudyListViewController.h"
#import "SignClassListViewController.h"

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *homeTableView;
@property (nonatomic,strong)NSArray *homeArr;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWihtInit];
    [self createTableView];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"测试";
}

#pragma mark - arrayWithInit
-(void)arrayWihtInit{
    self.homeArr = @[@"班级",@"箴言",@"学校",@"学生",@"作业",@"微信信息",@"考试信息",@"学习资料",@"签到"];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.homeTableView){
        self.homeTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.homeTableView.dataSource = self;
        self.homeTableView.delegate = self;
        [self.view addSubview:self.homeTableView];
    }
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.homeArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = 44;
    cellWithRowOne.transferTitle = [self.homeArr objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        ClassRootViewController *classRootViewController = [[ClassRootViewController alloc]init];
        [self.navigationController pushViewController:classRootViewController animated:YES];
    } else if (indexPath.row == 1){
        MottoListViewController *mottoListViewController = [[MottoListViewController alloc]init];
        [self.navigationController pushViewController:mottoListViewController animated:YES];
    } else if (indexPath.row == 2){
        SchoolListViewController *schoolVC = [[SchoolListViewController alloc]init];
        [self.navigationController pushViewController:schoolVC animated:YES];
    } else if (indexPath.row == 3){
        StudentListViewController *studentVC = [[StudentListViewController alloc]init];
        [self.navigationController pushViewController:studentVC animated:YES];
    } else if (indexPath.row == 4){
        WorkRootViewController *workListVC = [[WorkRootViewController alloc]init];
        [self.navigationController pushViewController:workListVC animated:YES];
    } else if (indexPath.row == 5){
        WechatNoticeListViewController *wechatVC = [[WechatNoticeListViewController alloc]init];
        [self.navigationController pushViewController:wechatVC animated:YES];
    } else if (indexPath.row == 6){
        ExamFilListViewController *examFileListVC = [[ExamFilListViewController alloc]init];
        [self.navigationController pushViewController:examFileListVC animated:YES];
    } else if (indexPath.row == 7){
        StudyListViewController *studyController = [[StudyListViewController alloc]init];
        [self.navigationController pushViewController:studyController animated:YES];
    } else if (indexPath.row == 8){
        SignClassListViewController *signVC = [[SignClassListViewController alloc]init];
        [self.navigationController pushViewController:signVC animated:YES];
    }
}

@end
