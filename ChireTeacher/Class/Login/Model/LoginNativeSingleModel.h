//
//  LoginNativeSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/19.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface LoginNativeSingleModel : FetchModel

// 【2】.用户信息
@property (nonatomic,copy)NSString *ID;                             // 编号
@property (nonatomic,copy)NSString *avatar;                         // 头像
@property (nonatomic,copy)NSString *nick;                           // 昵称
@property (nonatomic,assign)NSInteger sex;                               // 性别
@property (nonatomic,assign)NSInteger age;                          // 年龄
@property (nonatomic,copy)NSString *phone;                          // 手机
@property (nonatomic,copy)NSString *device;                         // 设备号
@property (nonatomic,copy)NSString *union_id;                       // qq登录的唯一号
@property (nonatomic,copy)NSString *user_sig;                       // 用户sig
@property (nonatomic,copy)NSString *im_id;                          // Im的ID
@property (nonatomic,copy)NSString *location;                       // 地址
@property (nonatomic,copy)NSString *city;                           // 地址
@property (nonatomic,copy)NSString *area;                           // 地址
@property (nonatomic,copy)NSString *lat;                            // 地址
@property (nonatomic,copy)NSString *lng;                            // 地址
@property (nonatomic,copy)NSString *live_relationId;                // 一对一关联账号
@property (nonatomic,copy)NSString *onebyone_relationId;            // 一对一关联账号
@property (nonatomic,assign)NSInteger money;                        // 金钱
@property (nonatomic,assign)NSInteger fans_count;                   // 粉丝数量
@property (nonatomic,assign)NSInteger link_count;                   // 关注数量
@property (nonatomic,copy)NSString *zhifupay;                       // 关注数量
@property (nonatomic,assign)LiveUserType user_type;                 // 用户类型
@property (nonatomic,copy)NSString *live_title;
@property (nonatomic,copy)NSString *live_img;
@property (nonatomic,copy)NSString *union_number;

@end
