//
//  AccountModel.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EnumConstance.h"
#import "PDThirdLoginModel.h"
#import "LoginNativeSingleModel.h"
#import "PDCurrentLocationManager.h"
#import "OSSSingleFileModel.h"

typedef NS_ENUM(NSInteger,AccountType) {
    AccountTypeGuest = 0,                   /**< 游客类型*/
    AccountTypeMember,                      /**< 注册用户类型*/
};

@interface AccountModel : FetchModel

+(instancetype)sharedAccountModel;                                                               /**< 单例*/

#pragma mark - 将要上传的d内容
@property (nonatomic,strong)OSSSingleFileModel *ossFileModel;









//
@property (nonatomic,assign)BOOL isShenhe;
@property (nonatomic,copy)NSString *sig;

// 【0.】用户信息
@property (nonatomic,copy)NSString *token;                                      /**< token*/

// 【4】.三方登录信息账号
@property (nonatomic,strong)PDThirdLoginModel *thirdUserModel;
@property (nonatomic,assign)PDThirdLoginType thirdLoginType;                    /**< 用户三方登录的类型*/

// 【1】.固定
@property (nonatomic,copy)NSString *lifeSocketHost;                             /**< 生命周期内的IP号*/
@property (nonatomic,assign)NSInteger lifeSocketPort;                           /**< 生命周期内的端口号*/
@property (nonatomic,assign)BOOL lifeIsShenhe;                                  /**< 判断是否审核*/
@property (nonatomic,strong)UIColor *styleColor;                                /**< 主色*/
@property (nonatomic,strong)UIColor *userChoose_StyleColor;                     /**< 用户选择的主色调*/

// 【3.】 登录信息
@property (nonatomic,strong)LoginNativeSingleModel *loginModel;                 /**< 登录Model*/

// 【4.location】
@property (nonatomic,strong)AMapAddressComponent *userLocation;
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,assign)CGFloat lng;
@property (nonatomic,copy)NSString *login_delegate;

// 一模狗
@property (nonatomic,copy)NSString *userToken;
@property (nonatomic,copy)NSString *userId;

@property (nonatomic,copy)NSString *deviceId;
@property (nonatomic,copy)NSString *headImg;
@property (nonatomic,copy)NSString *isLogin;
@property (nonatomic,copy)NSString *nickName;
@property (nonatomic,copy)NSString *osType;
@property (nonatomic,copy)NSString *payPasswd;
@property (nonatomic,copy)NSString *sex;
@property (nonatomic,copy)NSString *authToken;



-(void)loginManagerWithCallBack:(void(^)(BOOL successed))block;                 // 登录方法
-(void)loginManagerWithMyAccount:(NSString *)account callBack:(void(^)(BOOL successed))block;    // 三方登录方法

-(void)logoutManagerWithBlock:(void(^)())block;                                 // 退出登录方法


#pragma mark - login TX Cloud
-(void)sendRequestToTXCloudWithBlock:(void(^)(BOOL successed))block;
// 自动登录
-(void)autoLoginWithBlock:(void(^)(BOOL successed))block;

-(void)smartToAuthManagerWithBlock:(void(^)(BOOL successed))block;

#pragma mark - 判断【是否登录】
- (BOOL)hasLoggedIn;

-(void)reloadAllAccountInfo;

@end
