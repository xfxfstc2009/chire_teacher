//
//  AccountModel.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "AccountModel.h"

@implementation AccountModel

+(instancetype)sharedAccountModel{
    static AccountModel *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[AccountModel alloc] init];
    });
    return _sharedAccountModel;
}



#pragma mark - 退出登录方法
-(void)logoutManagerWithBlock:(void(^)())block{
  
}

// 自有账号登录
-(void)loginManagerWithMyAccount:(NSString *)account callBack:(void(^)(BOOL successed))block{
    [self loginManagerType:[AccountModel sharedAccountModel].thirdLoginType account:account callBack:^(BOOL successed) {
        if (block){
            block(successed);
        }
    }];
}


// 三方登录
-(void)loginManagerWithCallBack:(void(^)(BOOL successed))block{
    [self loginManagerType:[AccountModel sharedAccountModel].thirdLoginType account:[AccountModel sharedAccountModel].thirdUserModel.unionid callBack:^(BOOL successed) {
        if (block){
            block(successed);
        }
    }];
}


#pragma mark - 登录方法
-(void)loginManagerType:(PDThirdLoginType)loginType account:(NSString *)account callBack:(void(^)(BOOL successed))block{
   
}


#pragma mark - login TX Cloud
-(void)sendRequestToTXCloudWithBlock:(void(^)(BOOL successed))block{
    NSString *sig = @"";
    if ([AccountModel sharedAccountModel].loginModel.user_sig.length){
        sig = [AccountModel sharedAccountModel].loginModel.user_sig;
    }
    NSString *userId = @"";
    if ([AccountModel sharedAccountModel].thirdLoginType == PDThirdLoginTypeNor){
        userId = [AccountModel sharedAccountModel].loginModel.phone;
    } else {
        userId = [AccountModel sharedAccountModel].loginModel.union_id;
    }
    
   
}

#pragma mark - 自动登录
-(void)autoLoginWithBlock:(void(^)(BOOL successed))block{
    NSString *loginType = [Tool userDefaultGetWithKey:LoginType];
    NSString *loginKey = [Tool userDefaultGetWithKey:LoginKey];
    if (loginType.length && loginKey.length){
        PDThirdLoginType thirdLoginType = (PDThirdLoginType)[loginType integerValue];
        __weak typeof(self)weakSelf = self;
        [AccountModel sharedAccountModel].thirdLoginType = thirdLoginType;

        [self loginManagerType:thirdLoginType account:loginKey callBack:^(BOOL successed) {
            if (!weakSelf){
                return ;
            }
            if (block){
                block(successed);
            }
        }];
    }
}

#pragma mark - 提交用户的地址信息
-(void)uploadUserLocationManager{
    if ([AccountModel sharedAccountModel].userLocation){
        [self sendRequestToUplocationInfo];
    } else {
        __weak typeof(self)weakSelf = self;
        [[PDCurrentLocationManager sharedLocation] getCurrentLocationManager:^(CGFloat lat, CGFloat lng, AMapAddressComponent *addressComponent) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [AccountModel sharedAccountModel].lat = lat;
            [AccountModel sharedAccountModel].lng = lng;
            [AccountModel sharedAccountModel].userLocation = addressComponent;
            [strongSelf sendRequestToUplocationInfo];
        }];
    }
}

-(void)sendRequestToUplocationInfo{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([AccountModel sharedAccountModel].userLocation.province.length){
        [params setObject:[AccountModel sharedAccountModel].userLocation.province forKey:@"province"];
    }
    if ([AccountModel sharedAccountModel].userLocation.city.length){
        [params setObject:[AccountModel sharedAccountModel].userLocation.city forKey:@"city"];
    }
    if ([AccountModel sharedAccountModel].userLocation.district.length){
        [params setObject:[AccountModel sharedAccountModel].userLocation.district forKey:@"area"];
    }
    if ([AccountModel sharedAccountModel].lat != 0){
        [params setObject:@([AccountModel sharedAccountModel].lat) forKey:@"lat"];
    }
    if ([AccountModel sharedAccountModel].lng != 0){
        [params setObject:@([AccountModel sharedAccountModel].lng) forKey:@"lng"];
    }
    
  
}


- (BOOL)hasLoggedIn{
    return ([AccountModel sharedAccountModel].loginModel.ID.length ? YES : NO);
}

#pragma mark - 链接长连接
-(void)linkLongLink{
   
}


// action
#pragma mark - 登录方法
-(void)loginManagerSuccessBlock:(void(^)(BOOL issuccess))block{
   
}

-(void)smartToAuthManagerWithBlock:(void(^)(BOOL successed))block{
 
}


-(NSString *)userId{
    return @"42875";
}


-(void)reloadAllAccountInfo{
  
}

@end
