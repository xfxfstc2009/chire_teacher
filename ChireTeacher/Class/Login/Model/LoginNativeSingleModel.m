//
//  LoginNativeSingleModel.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/19.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LoginNativeSingleModel.h"

@implementation LoginNativeSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
