//
//  LoginRegisterViewControler.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "LoginRegisterViewControler.h"
#import "PDCountDownButton.h"

@interface LoginRegisterViewControler()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)UITableView *loginTableView;
@property (nonatomic,strong)NSArray *loginArr;
@property (nonatomic,strong)UIButton *sendButton;

@property (nonatomic,strong)UITextField *phoneInputField;
@property (nonatomic,strong)UITextField *smsCodeInputField;
@property (nonatomic,strong)UITextField *pwdInputField;
@property (nonatomic,strong)PDCountDownButton *daojishiButton;

@end

@implementation LoginRegisterViewControler

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.transferType == registerVCTypeChange){
        self.barMainTitle = @"修改密码";
    } else if (self.transferType == registerVCTypeRegister){
        self.barMainTitle = @"账号注册";
    } else if (self.transferType == registerVCTypeReset){
        self.barMainTitle = @"重置密码";
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.loginArr = @[@[@"输入手机号",@"输入验证码",@"设置新的密码"],@[@"提交"]];
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.loginTableView){
        self.loginTableView = [[UITableView alloc] initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.loginTableView.delegate = self;
        self.loginTableView.dataSource = self;
        self.loginTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.loginTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.loginTableView.showsVerticalScrollIndicator = YES;
        self.loginTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.loginTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.loginArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.loginArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        if (indexPath.row == 0 || indexPath.row == 2){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                
                UITextField *inputTextField = [[UITextField alloc]init];
                inputTextField.backgroundColor = [UIColor clearColor];
                inputTextField.stringTag = @"inputTextField";
                inputTextField.delegate = self;
                [inputTextField addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
                inputTextField.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), cellHeight);
                [cellWithRowOne addSubview:inputTextField];
            }
            UITextField *inputTextField = (UITextField *)[cellWithRowOne viewWithStringTag:@"inputTextField"];
            if (indexPath.row == 0){
                inputTextField.placeholder = @"请输入手机号";
                self.phoneInputField = inputTextField;
                self.phoneInputField.keyboardType = UIKeyboardTypeNumberPad;
                self.phoneInputField.returnKeyType = UIReturnKeyContinue;
            } else if (indexPath.row == 2){
                inputTextField.placeholder = @"请设置新的密码";
                self.pwdInputField = inputTextField;
                self.pwdInputField.keyboardType = UIKeyboardTypeEmailAddress;
                self.pwdInputField.returnKeyType = UIReturnKeyDone;
                self.pwdInputField.secureTextEntry = YES;
            }
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if(!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                
                // 1. 创建验证码输入
                self.smsCodeInputField = [[UITextField alloc]init];
                self.smsCodeInputField.backgroundColor = [UIColor clearColor];
                self.smsCodeInputField.frame = CGRectMake(LCFloat(15), 0, kScreenBounds.size.width - 2 * LCFloat(15), cellHeight);
                self.smsCodeInputField.placeholder = @"请输入验证码";
                self.smsCodeInputField.delegate = self;
                [self.smsCodeInputField addTarget:self action:@selector(textFieldDidChanged:) forControlEvents:UIControlEventEditingChanged];
                [cellWithRowTwo addSubview:self.smsCodeInputField];
                
                // 2. 创建倒计时
                __weak typeof(self)weakSelf = self;
                self.daojishiButton = [[PDCountDownButton alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(15) - LCFloat(90), LCFloat(8), LCFloat(90), (cellHeight - LCFloat(8) * 2)) daojishi:60 withBlock:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf.daojishiButton startTimer];
                }];
                self.daojishiButton.clipsToBounds = YES;
                self.daojishiButton.layer.cornerRadius = LCFloat(4);
                [self.daojishiButton smsLabelTypeManager:NO];
                [cellWithRowTwo addSubview:self.daojishiButton];
            }
            return cellWithRowTwo;
        }
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowThr.backgroundColor = [UIColor clearColor];
            
            // 确定发送按钮
            self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.sendButton setTitle:@"提交" forState:UIControlStateNormal];
            self.sendButton.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
            self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
            self.sendButton.layer.cornerRadius = LCFloat(5);
            self.sendButton.frame = CGRectMake(LCFloat(16), 0, kScreenBounds.size.width - 2 * LCFloat(16), cellHeight);
            self.sendButton.enabled = NO;
            __weak typeof(self)weakSelf = self;
            [self.sendButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
  //              __strong typeof(weakSelf)strongSelf = weakSelf;
                NSLog(@"提交");
            }];
            [cellWithRowThr addSubview: self.sendButton];
        }
        return cellWithRowThr;
    }
}


#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return LCFloat(50);
    } else {
        return LCFloat(55);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.loginTableView) {
        if (indexPath.section == 0){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.loginArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
            }
            if ([[self.loginArr objectAtIndex:indexPath.section] count] == 1) {
                separatorType  = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (textField == self.phoneInputField){                     // 【验证码】
        if (toBeString.length > 11){
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
    } else if (textField == self.smsCodeInputField){
        if(toBeString.length > 6){
            textField.text = [toBeString substringToIndex:6];
            return NO;
        }
    } else if (textField == self.pwdInputField){
        if (toBeString.length > 18){
            textField.text = [toBeString substringToIndex:18];
            return NO;
        }
    }
    return YES;
}

-(void)textFieldDidChanged:(UITextField *)textField{
    if (textField == self.phoneInputField){
        if ([Tool validateMobile:textField.text]){
            [self.daojishiButton smsLabelTypeManager:YES];
        } else {
            [self.daojishiButton smsLabelTypeManager:NO];
        }
    } else if (textField == self.smsCodeInputField){
    } else if (textField == self.pwdInputField){

    }
    [self senderInfoManager];
}



#pragma mark - senderEnable
-(void)senderInfoManager{
    if ([Tool validateMobile:self.phoneInputField.text] && self.smsCodeInputField.text.length >= 6 && [Tool validatePassword:self.pwdInputField.text]){
        [self senderBtnStatus:YES];
    } else {
        [self senderBtnStatus:NO];
    }
}

-(void)senderBtnStatus:(BOOL)status{
    if (status == YES){
        self.sendButton.enabled = YES;
        self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"绿"];
    } else {
        self.sendButton.enabled = NO;
        self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
    }
}

@end
