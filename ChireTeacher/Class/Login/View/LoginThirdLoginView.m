//
//  LoginThirdLoginView.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/14.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "LoginThirdLoginView.h"

static char actionButtonClickKey;
@interface LoginThirdLoginView()
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *lineView2;
@property (nonatomic,strong)UIView *itemsView;

@end

@implementation LoginThirdLoginView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"灰"];
    self.titleLabel.text = @"其他方式登录";
    CGSize titleSize = [NSString makeSizeWithLabel:self.titleLabel];
    [self addSubview:self.titleLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview:self.lineView];
    
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview:self.lineView2];
    
    self.titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2., 0, titleSize.width, titleSize.height);
    
    CGFloat lineWidth = (kScreenBounds.size.width - 4 * LCFloat(11) - titleSize.width) / 2.;
    self.lineView.frame = CGRectMake(LCFloat(11), 0, lineWidth, .5f);
    self.lineView.center_y = self.titleLabel.center_y;
    
    self.lineView2.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(11), self.lineView.orgin_y, self.lineView.size_width, .5f);
    [self addSubview:self.lineView2];
}

-(void)setTransferArr:(NSArray *)transferArr{
    _transferArr = transferArr;
    
    for (int i = 0 ; i < transferArr.count;i++){
        NSString *info = [self.transferArr objectAtIndex:i];
        
        UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        if ([info isEqualToString:@"wechat"]){
            [actionButton setTitle:@"微信登录" forState:UIControlStateNormal];
            [actionButton setImage:[UIImage imageNamed:@"login_thirdWechat_icon"] forState:UIControlStateNormal];
        } else if ([info isEqualToString:@"qq"]){
            [actionButton setTitle:@"QQ登录" forState:UIControlStateNormal];
            [actionButton setImage:[UIImage imageNamed:@"login_thirdQQ_icon"] forState:UIControlStateNormal];
        }
        actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
        [actionButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        __weak typeof(self)weakSelf = self;
        [actionButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &actionButtonClickKey);
            if (block){
                block(info);
            }
        }];
        actionButton.imageView.bounds = CGRectMake(0, 0, LCFloat(50), LCFloat(50));
        CGFloat size_width = LCFloat(60);
        CGFloat size_height = LCFloat(60) + LCFloat(11) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
        CGFloat origin_x = (kScreenBounds.size.width - 2 * LCFloat(60)) / 3.;
        CGFloat origin_y = CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11);
        actionButton.frame = CGRectMake(origin_x + (origin_x + size_width) * i, origin_y, size_width, size_height);
        [actionButton layoutButtonWithEdgeInsetsStyle:ButtonEdgeInsetsStyleTop imageTitleSpace:LCFloat(5)];
        [self addSubview:actionButton];
    }
}

-(void)actionButtonClick:(void(^)(NSString *key))block{
    objc_setAssociatedObject(self, &actionButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
