//
//  StudentModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StudentModel <NSObject>

@end


@interface StudentModel : FetchModel

@property (nonatomic,assign)NSInteger age;
@property (nonatomic,assign)NSInteger authStatus;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *classid;
@property (nonatomic,copy)NSString *gender;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *infomation;
@property (nonatomic,copy)NSString *jointime;
@property (nonatomic,copy)NSString *mark;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *openid;
@property (nonatomic,copy)NSString *parentnumber;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,copy)NSString *qq;
@property (nonatomic,copy)NSString *school;
@property (nonatomic,copy)NSString *snumber;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *unionid;
@property (nonatomic,copy)NSString *wechat;

@end


@interface StudentListModel : FetchModel

@property (nonatomic,strong)NSArray<StudentModel>*list;

@end
NS_ASSUME_NONNULL_END
