//
//  NetworkAdapter+Student.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/15.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter+Student.h"

@implementation NetworkAdapter (Student)


#pragma mark - 获取所有的学生
-(void)getAllStudentListWithBlock:(void(^)(StudentListModel *studentList))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Student_List requestParams:nil responseObjectClass:[StudentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            StudentListModel *listModel = (StudentListModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}

#pragma mark - 获取所有班级内的学生
-(void)getAllStudentListWithClassId:(NSString *)classId block:(void(^)(StudentListModel *studentList))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params;
    if (!classId.length){
        [StatusBarManager statusBarHidenWithText:@"请选择班级"];
        return;
    }
    params = @{@"classid":classId};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:Student_List requestParams:params responseObjectClass:[StudentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            StudentListModel *listModel = (StudentListModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}

#pragma mark - 获取学生信息
-(void)getStudentInfoWithStudentId:(NSString *)studentId block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"id":studentId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:Student_Detail requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        
    }];
}


@end
