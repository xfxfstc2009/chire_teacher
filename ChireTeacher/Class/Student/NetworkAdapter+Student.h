//
//  NetworkAdapter+Student.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/15.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter.h"
#import "StudentModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Student)
#pragma mark - 获取所有的学生
-(void)getAllStudentListWithBlock:(void(^)(StudentListModel *studentList))block;
#pragma mark - 获取所有班级内的学生
-(void)getAllStudentListWithClassId:(NSString *)classId block:(void(^)(StudentListModel *studentList))block;
#pragma mark - 获取学生信息
-(void)getStudentInfoWithStudentId:(NSString *)studentId block:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
