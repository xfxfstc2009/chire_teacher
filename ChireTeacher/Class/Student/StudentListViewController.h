//
//  StudentListViewController.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface StudentListViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferClassId;

@end

NS_ASSUME_NONNULL_END
