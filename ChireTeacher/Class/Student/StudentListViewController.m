//
//  StudentListViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "StudentListViewController.h"
#import "StudentModel.h"
#import "NetworkAdapter+Class.h"
#import "StudentDetailViewController.h"

@interface StudentListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *studentTableView;
@property (nonatomic,strong)NSMutableArray *studentMutableArr;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;
@property (nonatomic,strong)NSMutableArray *segmentTitleMutableArr;
@end

@implementation StudentListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSegment];
    [self createTableView];
    [self getAllClassList];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"学生列表";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.studentMutableArr = [NSMutableArray array];
    self.segmentMutableArr = [NSMutableArray array];
    self.segmentTitleMutableArr = [NSMutableArray array];
}

#pragma mark - createSegment
-(void)createSegment{
    __weak typeof(self)weakSelf = self;
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:self.segmentTitleMutableArr actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf =weakSelf;
        NSString *title = [self.segmentTitleMutableArr objectAtIndex:index];
        NSString *classId = @"";
        for (int i = 0 ;i<self.segmentMutableArr.count;i++){
            ClassModel *classModel = [self.segmentMutableArr objectAtIndex:i];
            if ([classModel.name isEqualToString:title]){
                classId = classModel.ID;
                break;
            }
        }
        
        strongSelf.transferClassId = classId;
        [strongSelf sendRequestToGetInfo];
    }];
    self.segmentList.isNotScroll = NO;
    self.segmentList.backgroundColor = [UIColor whiteColor];
    self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44));
    [self.view addSubview:self.segmentList];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.studentTableView){
        self.studentTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.studentTableView.orgin_y = CGRectGetMaxY(self.segmentList.frame);
        self.studentTableView.size_height -= CGRectGetMaxY(self.segmentList.frame);
        self.studentTableView.dataSource = self;
        self.studentTableView.delegate = self;
        [self.view addSubview:self.studentTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.studentMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = 44;
    StudentModel *studentModel = [self.studentMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = studentModel.name;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    StudentDetailViewController *studentVC = [[StudentDetailViewController alloc]init];
    [self.navigationController pushViewController:studentVC animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(7);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType  = SeparatorTypeHead;
    } else if ([indexPath row] == [self.studentMutableArr count] - 1) {
        separatorType  = SeparatorTypeBottom;
    } else {
        separatorType  = SeparatorTypeMiddle;
    }
    if ([self.studentMutableArr count] == 1) {
        separatorType  = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithType:separatorType];
}


-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params;
    if (self.transferClassId.length){
        params = @{@"classid":self.transferClassId};
    } else {
        params = nil;
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:Student_List requestParams:params responseObjectClass:[StudentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.studentMutableArr removeAllObjects];
            
            StudentListModel *listModel = (StudentListModel *)responseObject;
            [strongSelf.studentMutableArr addObjectsFromArray:listModel.list];
            [strongSelf.studentTableView reloadData];
            
            if (strongSelf.studentMutableArr.count){
                [strongSelf.studentTableView dismissPrompt];
            } else {
                [strongSelf.studentTableView showPrompt:@"当前班级没有学生" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
        }
    }];
}

#pragma mark - 获取所有的班级
-(void)getAllClassList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] classListAllBlock:^(ClassListModel * _Nonnull classListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.segmentTitleMutableArr addObject:@"所有学生"];
        for (int i = 0 ; i < classListModel.list.count;i++){
            ClassModel *classModel = [classListModel.list objectAtIndex:i];
            [strongSelf.segmentMutableArr addObject:classModel];
            [strongSelf.segmentTitleMutableArr addObject:classModel.name];
        }
        [strongSelf.segmentList reloadData];
    }];
}

@end

