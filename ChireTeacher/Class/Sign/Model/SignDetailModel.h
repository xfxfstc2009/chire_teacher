//
//  SignDetailModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/18.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SignDetailModel <NSObject>

@end

@interface SignDetailModel : FetchModel

@property (nonatomic,copy)NSString *class_id;
@property (nonatomic,copy)NSString *date;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *sign_type;
@property (nonatomic,copy)NSString *student_id;
@property (nonatomic,copy)NSString *time;

@end

@interface SignDetailListModel : FetchModel

@property (nonatomic,strong)NSArray<SignDetailModel> *list;


@end


NS_ASSUME_NONNULL_END
