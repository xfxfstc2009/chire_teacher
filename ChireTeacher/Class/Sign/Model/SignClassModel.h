//
//  SignClassModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/18.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"
#import "ClassModel.h"

NS_ASSUME_NONNULL_BEGIN



@protocol SignClassModel <NSObject>


@end

@interface SignClassModel : FetchModel

@property (nonatomic,copy)NSString *class_id;
@property (nonatomic,copy)NSString *date;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *linkId;
@property (nonatomic,assign)NSTimeInterval dateTimeInterval;
@property (nonatomic,strong)ClassModel *classModel;
@end


@interface SignClassListModel : FetchModel

@property (nonatomic,strong)NSArray<SignClassModel> *list;
@end
NS_ASSUME_NONNULL_END
