//
//  SignClassListViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/18.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "SignClassListViewController.h"
#import "SignClassModel.h"
#import "SignDetailViewController.h"

@interface SignClassListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *signClassTableView;
@property (nonatomic,strong)NSMutableArray *signClassMutableArr;
@end

@implementation SignClassListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"签到";
}

#pragma mark - pageSetting
-(void)arrayWithInit{
    self.signClassMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.signClassTableView){
        self.signClassTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.signClassTableView.dataSource = self;
        self.signClassTableView.delegate = self;
        [self.view addSubview:self.signClassTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.signClassMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    SignClassModel *signClassModel = [self.signClassMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = signClassModel.classModel.name;
    cellWithRowOne.transferDesc = [NSDate getTimeWithLotteryString:signClassModel.dateTimeInterval];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SignDetailViewController *signDetailVC = [[SignDetailViewController alloc]init];
    SignClassModel *signClassModel = [self.signClassMutableArr objectAtIndex:indexPath.row];
    signDetailVC.transferLinkId = signClassModel.linkId;
    [self.navigationController pushViewController:signDetailVC animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(44);
}

#pragma mark - Interface
-(void)sendRequestToGetList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Sign_List requestParams:nil responseObjectClass:[SignClassListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            SignClassListModel *listModel = (SignClassListModel *)responseObject;
            [strongSelf.signClassMutableArr addObjectsFromArray:listModel.list];
            [strongSelf.signClassTableView reloadData];
            if (strongSelf.signClassMutableArr.count){
                [strongSelf.signClassTableView dismissPrompt];
            } else {
                [strongSelf.signClassTableView showPrompt:@"当前没有信息" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
        }
    }];
}
@end
