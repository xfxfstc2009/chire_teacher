//
//  SignDetailViewController.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/18.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignDetailViewController : AbstractViewController
@property (nonatomic,copy)NSString *transferLinkId;
@end

NS_ASSUME_NONNULL_END
