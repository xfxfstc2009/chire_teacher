//
//  SignDetailViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/18.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "SignDetailViewController.h"
#import "SignDetailModel.h"

@interface SignDetailViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)NSMutableArray *signMutableArr;;
@property (nonatomic,strong)UITableView *signTableView;
@end

@implementation SignDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"签到详情";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.signMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.signTableView){
        self.signTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.signTableView.dataSource = self;
        self.signTableView.delegate = self;
        [self.view addSubview:self.signTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.signMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    SignDetailModel *sinleModel = [self.signMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if ([sinleModel.sign_type isEqualToString:@"0"]){
        cellWithRowOne.transferTitle = @"未签到";
    } else if ([sinleModel.sign_type isEqualToString:@"1"]){
        cellWithRowOne.transferTitle = @"已签到";
    } else if ([sinleModel.sign_type isEqualToString:@"2"]){
        cellWithRowOne.transferTitle = @"未签退";
    } else if ([sinleModel.sign_type isEqualToString:@"3"]){
        cellWithRowOne.transferTitle = @"已签退";
    }
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(44);
}

#pragma mark - nterface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"linkId":self.transferLinkId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:Sign_Class_Detail requestParams:params responseObjectClass:[SignDetailListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            SignDetailListModel *signDetailListModel = (SignDetailListModel *)responseObject;
            [strongSelf.signMutableArr addObjectsFromArray:signDetailListModel.list];
            [strongSelf.signTableView reloadData];
        }
    }];
}

@end
