//
//  ClassModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"
#import "TeacherModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ClassModel <NSObject>

@end

@interface ClassModel : FetchModel

@property (nonatomic,copy)NSString *begintime;              /**< 开始时间*/
@property (nonatomic,copy)NSString *classtime;                  /**< 开始时间*/
@property (nonatomic,copy)NSString *datetime;                  /**< 开始时间*/
@property (nonatomic,copy)NSString *deletestatus;              /**< 删除状态*/
@property (nonatomic,copy)NSString *endtime;                  /**< 开始时间*/
@property (nonatomic,copy)NSString *ID;                  /**< 开始时间*/
@property (nonatomic,copy)NSString *marks;                  /**< 开始时间*/
@property (nonatomic,copy)NSString *name;                  /**< 开始时间*/
@property (nonatomic,copy)NSString *outclasstime;                  /**< 开始时间*/
@property (nonatomic,copy)NSString *status;                  /**< 开始时间*/
@property (nonatomic,copy)NSString *week;                  /**< 开始时间*/
@property (nonatomic,strong)TeacherModel *teacher;
@property (nonatomic,copy)NSString *uploads;

@end


@interface ClassListModel : FetchModel

@property (nonatomic,strong)NSArray<ClassModel> *list;

@end


NS_ASSUME_NONNULL_END
