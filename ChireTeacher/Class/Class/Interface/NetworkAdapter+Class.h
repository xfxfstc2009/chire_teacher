//
//  NetworkAdapter+Class.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter.h"
#import "ClassModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Class)

// 获取所有的班级列表
-(void)classListAllBlock:(void(^)(ClassListModel *classListModel))block;

// 添加班级
-(void)classAddManagerWithName:(NSString *)className teacher:(NSString *)teacher beginTime:(NSString *)beginTime classTime:(NSString *)classTime marks:(NSString *)marks upload:(NSString *)upload status:(NSString *)status uploads:(NSString *)uploads week:(NSString *)week;

// 删除班级
-(void)classDeleteWithClassId:(NSString *)classId deleteBlock:(void(^)())block;
@end

NS_ASSUME_NONNULL_END
