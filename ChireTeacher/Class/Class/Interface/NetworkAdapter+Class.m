//
//  NetworkAdapter+Class.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter+Class.h"

@implementation NetworkAdapter (Class)

-(void)classListAllBlock:(void(^)(ClassListModel *classListModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Class_List requestParams:nil responseObjectClass:[ClassListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            ClassListModel *classModel = (ClassListModel *)responseObject;
            if (block){
                block(classModel);
            }
        }
    }];
}


#pragma mark - 添加班级
-(void)classAddManagerWithName:(NSString *)className teacher:(NSString *)teacher beginTime:(NSString *)beginTime classTime:(NSString *)classTime marks:(NSString *)marks upload:(NSString *)upload status:(NSString *)status uploads:(NSString *)uploads week:(NSString *)week{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *error = @"";
    if (!className.length){
        error = @"请输入班级名称";
    } else if (!teacher.length){
        error = @"请选择授课老师";
    } else if (!beginTime.length){
        error = @"请选择开课与结束时间";
    } else if (!classTime.length){
        error = @"请添加课程时间";
    } else if (!marks.length){
        error = @"请添加备注";
    } else if (!week.length){
        error = @"请添加每周几上课";
    }
    
    if (error.length){
        [StatusBarManager statusBarHidenWithText:error];
        return;
    }
    [params setValue:className forKey:@"name"];
    [params setValue:teacher forKey:@"teacher"];
    [params setValue:beginTime forKey:@"betime"];
    [params setValue:classTime forKey:@"classtime"];
    [params setValue:marks forKey:@"marks"];
    [params setValue:uploads forKey:@"uploads"];
    [params setValue:week forKey:@"week"];
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Class_Add requestParams:params  responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
    }];
    
}

#pragma mark - 删除班级
-(void)classDeleteWithClassId:(NSString *)classId deleteBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"id":classId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:Class_Del requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            
        }
    }];
}
@end
