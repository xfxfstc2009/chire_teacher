//
//  ClassRootViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "ClassRootViewController.h"
#import "NetworkAdapter+Class.h"
#import "StudentListViewController.h"
#import "ClassAddViewController.h"

@interface ClassRootViewController()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *classTableView;
@property (nonatomic,strong)NSMutableArray *classMutableArr;

@end

@implementation ClassRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetClassInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"班级";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        ClassAddViewController *classAddVC = [[ClassAddViewController alloc]init];
        [strongSelf.navigationController pushViewController:classAddVC animated:YES];
    }];
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.classMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.classTableView){
        self.classTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.classTableView.dataSource = self;
        self.classTableView.delegate = self;
        [self.view addSubview:self.classTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.classMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = LCFloat(44);
    ClassModel *classModel = [self.classMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = classModel.name;
    cellWithRowOne.transferHasArrow = YES;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWNormalTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    StudentListViewController *studentListViewController = [[StudentListViewController alloc]init];
    ClassModel *classModel = [self.classMutableArr objectAtIndex:indexPath.row];
    studentListViewController.transferClassId = classModel.ID;
    [self.navigationController pushViewController:studentListViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.classTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.classMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.classMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// 定义编辑样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self)weakSelf = self;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        ClassModel *classModel = [self.classMutableArr objectAtIndex:indexPath.row];
        [[UIAlertView alertViewWithTitle:@"是否删除" message:@"删除班级会导致班内同学无法正常接收开课提醒，是否进行删除？" buttonTitles:@[@"删除",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (buttonIndex == 0){
                [strongSelf sendRequestToDeleteClass:classModel.ID];
            }
        }] show];
    }

}

// 修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}



#pragma mark - Interface
// 获取班级列表
-(void)sendRequestToGetClassInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] classListAllBlock:^(ClassListModel * _Nonnull classListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.classMutableArr removeAllObjects];
        if (classListModel.list.count){
            [strongSelf.classMutableArr addObjectsFromArray:classListModel.list];
        }
        [strongSelf.classTableView reloadData];
    }];
}

// 删除班级
-(void)sendRequestToDeleteClass:(NSString *)classId{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] classDeleteWithClassId:classId deleteBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"删除成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [strongSelf sendRequestToGetClassInfo];
        }]show];
    }];
}
@end
