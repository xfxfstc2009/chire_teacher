//
//  ClassRootViewController.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ClassRootViewController : AbstractViewController

@end

NS_ASSUME_NONNULL_END
