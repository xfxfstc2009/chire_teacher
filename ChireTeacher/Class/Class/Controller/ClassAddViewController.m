//
//  ClassAddViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "ClassAddViewController.h"
#import "TeacherRootViewController.h"
#import "NetworkAdapter+Class.h"

@interface ClassAddViewController ()<UITableViewDelegate,UITableViewDataSource>{
    TeacherModel * selectedTeacher;
    UITextField *classNameTextField;
    UITextField *beginTimeTextField;
    UITextField *endTimeTextField;
    UITextField *countClassTimeTextField;               /**< 总课时*/
    UITextField *beizhuTextField;                       /**< 备注*/
}
@property (nonatomic,strong)UITableView *classTableView;
@property (nonatomic,strong)NSArray *classAddArr;
@property (nonatomic,strong)UIDatePicker *beginTimePicker;
@property (nonatomic,strong)UIDatePicker *endTimePicker;
@end

@implementation ClassAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self beginDatePicker];
    [self endDatePicker];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"班级添加";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.classAddArr = @[@[@"班级名称"],@[@"选择老师"],@[@"开始时间",@"结束时间"],@[@"总课时"],@[@"备注"],@[@"确认添加"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.classTableView){
        self.classTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.classTableView.dataSource = self;
        self.classTableView.delegate = self;
        [self.view addSubview:self.classTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.classAddArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.classAddArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"班级名称" sourceArr:self.classAddArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        classNameTextField = cellWithRowOne.inputTextField;
        cellWithRowOne.transferTitle = @"班级名称";
        cellWithRowOne.transferPlaceholeder = @"请输入班级名称";
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSLog(@"%@",info);
        }];
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"选择老师" sourceArr:self.classAddArr]){
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        GWNormalTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        }
        cellWithRowOther.transferCellHeight = cellHeight;
        cellWithRowOther.transferTitle = [[self.classAddArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowOther.transferHasArrow = YES;
        return cellWithRowOther;
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"开始时间" sourceArr:self.classAddArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"开始时间" sourceArr:self.classAddArr]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"结束时间" sourceArr:self.classAddArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"结束时间" sourceArr:self.classAddArr])){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"开始时间" sourceArr:self.classAddArr]){
            cellWithRowTwo.transferTitle = @"开始时间";
            cellWithRowTwo.transferPlaceholeder = @"请选择开始时间";
            beginTimeTextField = cellWithRowTwo.inputTextField;
            beginTimeTextField.inputView = self.beginTimePicker;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"结束时间" sourceArr:self.classAddArr]){
            cellWithRowTwo.transferTitle = @"结束时间";
            cellWithRowTwo.transferPlaceholeder = @"请选择结束时间";
            endTimeTextField = cellWithRowTwo.inputTextField;
            endTimeTextField.inputView = self.endTimePicker;
        }
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"总课时" sourceArr:self.classAddArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWInputTextFieldTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
      
        cellWithRowThr.transferTitle = @"总课时";
        cellWithRowThr.transferPlaceholeder = @"请输入总课时";
        countClassTimeTextField = cellWithRowThr.inputTextField;
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"备注" sourceArr:self.classAddArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWInputTextFieldTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        
        cellWithRowFour.transferTitle = @"备注";
        cellWithRowFour.transferPlaceholeder = @"请输入备注";
        beizhuTextField = cellWithRowFour.inputTextField;
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"确认添加" sourceArr:self.classAddArr]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        GWButtonTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        cellWithRowFiv.transferTitle = @"确认添加";
        __weak typeof(self)weakSelf = self;
        [cellWithRowFiv buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToAddClass];
        }];
        return cellWithRowFiv;
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        GWNormalTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        }
        cellWithRowOther.transferCellHeight = cellHeight;
        cellWithRowOther.transferTitle = [[self.classAddArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowOther.transferHasArrow = YES;
        return cellWithRowOther;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"选择老师" sourceArr:self.classAddArr]){
        TeacherRootViewController *teacherVC = [[TeacherRootViewController alloc]init];
        teacherVC.transferType = TeacherRootViewControllerTypeSelected;
        __weak typeof(self)weakSelf = self;
        [teacherVC actionClickWithSelectedTeacherBlock:^(TeacherModel * _Nonnull teaceherModel) {
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->selectedTeacher = teaceherModel;
        }];
        [self.navigationController pushViewController:teacherVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"开始时间" sourceArr:self.classAddArr]){
        [beginTimeTextField becomeFirstResponder];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

#pragma mark - DatePicker
-(UIDatePicker *)createDatePicker{
    UIDatePicker *timePicker = [ [ UIDatePicker alloc] initWithFrame:CGRectMake(0,0,kScreenBounds.size.width,216)];
    [timePicker setDatePickerMode:UIDatePickerModeTime];
    timePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    [timePicker addTarget:self action:@selector(dateChange:)forControlEvents:UIControlEventValueChanged];
    return timePicker;
}

-(UIDatePicker *)beginDatePicker{
    if (!self.beginTimePicker){
       self.beginTimePicker = [self createDatePicker];
    }
    return self.beginTimePicker;
}

-(UIDatePicker *)endDatePicker{
    if (!self.endTimePicker){
        self.endTimePicker = [self createDatePicker];
    }
    return self.endTimePicker;
}


-(void)dateChange:(UIDatePicker *)sender{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    //设置时间格式
    formatter.dateFormat = @"HH:mm:ss";
    NSString *dateStr = [formatter  stringFromDate:sender.date];
    if (sender == self.beginTimePicker){
        beginTimeTextField.text = dateStr;
    } else if (sender == self.endTimePicker){
        endTimeTextField.text = dateStr;
    }
}


#pragma mark - InterfaceManager
-(void)sendRequestToAddClass{
    
    NSString *beTime = [NSString stringWithFormat:@"%@-%@",beginTimeTextField.text,endTimeTextField.text];
    [[NetworkAdapter sharedAdapter] classAddManagerWithName:classNameTextField.text teacher:selectedTeacher.ID beginTime:beTime classTime:countClassTimeTextField.text marks:@"SMa" upload:@"123" status:@"1" uploads:@"1" week:@"1"];
}
@end
