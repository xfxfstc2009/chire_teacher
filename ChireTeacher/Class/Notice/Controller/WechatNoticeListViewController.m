//
//  WechatNoticeListViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/15.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "WechatNoticeListViewController.h"
#import "NetworkAdapter+Notice.h"

@interface WechatNoticeListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *noticeTableView;
@property (nonatomic,strong)NSMutableArray *noticeMutableArr;
@end

@implementation WechatNoticeListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.noticeMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.noticeTableView){
        self.noticeTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.noticeTableView.dataSource = self;
        self.noticeTableView.delegate = self;
        [self.view addSubview:self.noticeTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.noticeMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = 44;
    WechatNoticeModel *noticeModel = [self.noticeMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = noticeModel.info;
    cellWithRowOne.transferDesc = noticeModel.datetime;
    cellWithRowOne.transferHasArrow = YES;
    return cellWithRowOne;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.noticeTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.noticeMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.noticeMutableArr count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - Interface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToGetNoticeListBlock:^(WechatNoticeListModel * _Nonnull listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.noticeMutableArr removeAllObjects];
        [strongSelf.noticeMutableArr addObjectsFromArray:listModel.list];
        [strongSelf.noticeTableView reloadData];
    }];
}




@end
