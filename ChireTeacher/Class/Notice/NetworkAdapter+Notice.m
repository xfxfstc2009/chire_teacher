//
//  NetworkAdapter+Notice.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/15.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter+Notice.h"

@implementation NetworkAdapter (Notice)

-(void)sendRequestToGetNoticeListBlock:(void(^)(WechatNoticeListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page":@"1"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:wechat_notice_list requestParams:params responseObjectClass:[WechatNoticeListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            WechatNoticeListModel *listModel = (WechatNoticeListModel *)responseObject;
            if (block){
                block(listModel);
            }
        }
    }];
}

@end
