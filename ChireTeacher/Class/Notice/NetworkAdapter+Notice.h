//
//  NetworkAdapter+Notice.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/15.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter.h"
#import "WechatNoticeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Notice)

-(void)sendRequestToGetNoticeListBlock:(void(^)(WechatNoticeListModel *listModel))block;

@end

NS_ASSUME_NONNULL_END
