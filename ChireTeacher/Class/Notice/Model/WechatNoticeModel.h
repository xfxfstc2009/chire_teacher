//
//  WechatNoticeModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/15.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol WechatNoticeModel <NSObject>

@end

@interface WechatNoticeModel : FetchModel

@property (nonatomic,copy)NSString *datetime;
@property (nonatomic,copy)NSString *info;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *openid;

@end

@interface WechatNoticeListModel : FetchModel

@property (nonatomic,strong)NSArray<WechatNoticeModel> *list;

@end



NS_ASSUME_NONNULL_END
