//
//  MottoAddViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "MottoAddViewController.h"

@interface MottoAddViewController()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *mottoTextField;
}
@property (nonatomic,strong)UITableView *mottoTableView;
@property (nonatomic,strong)NSArray *mottoArr;

@end

@implementation MottoAddViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加箴言";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.mottoArr = @[@[@"添加箴言"],@[@"按钮"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mottoTableView){
        self.mottoTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.mottoTableView.dataSource = self;
        self.mottoTableView.delegate = self;
        [self.view addSubview:self.mottoTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.mottoArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.mottoArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"箴言";
        cellWithRowOne.transferPlaceholeder = @"请添加箴言";
        mottoTextField = cellWithRowOne.inputTextField;
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->mottoTextField.text = info;
        }];
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"添加箴言";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToAddMotto];
        }];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [GWInputTextFieldTableViewCell calculationCellHeight];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)sendRequestToAddMotto{
    __weak typeof(self)weakSelf = self;
    if (!mottoTextField.text.length){
        [StatusBarManager statusBarHidenWithText:@"请输入箴言"];
        return;
    }
    NSDictionary *params = @{@"motto":mottoTextField.text};
    [[NetworkAdapter sharedAdapter] fetchWithPath:Motto_Add requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"添加成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }]show];
    }];
}
@end
