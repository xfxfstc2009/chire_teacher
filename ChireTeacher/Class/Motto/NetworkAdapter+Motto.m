
//
//  NetworkAdapter+Motto.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter+Motto.h"

@implementation NetworkAdapter (Motto)

-(void)mottoDeleteWithmotto:(NSString *)motto block:(void(^)())block{
    __weak typeof(self)wealSelf = self;
    NSDictionary *params = @{@"motto":motto};
    [[NetworkAdapter sharedAdapter] fetchWithPath:Motto_Del requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!wealSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}

@end
