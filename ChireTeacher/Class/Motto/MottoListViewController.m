//
//  MottoListViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "MottoListViewController.h"
#import "MottoModel.h"
#import "MottoSingleTableViewCell.h"
#import "MottoAddViewController.h"
#import "NetworkAdapter+Motto.h"

@interface MottoListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *mottoTableView;
@property (nonatomic,strong)NSMutableArray *mottoMutableArr;
@end

@implementation MottoListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInterface];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"箴言";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        MottoAddViewController *mottoAddVC = [[MottoAddViewController alloc]init];
        [strongSelf.navigationController pushViewController:mottoAddVC animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.mottoMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mottoTableView){
        self.mottoTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.mottoTableView.dataSource = self;
        self.mottoTableView.delegate = self;
        [self.view addSubview:self.mottoTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mottoMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    MottoSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[MottoSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferInfo = [self.mottoMutableArr objectAtIndex:indexPath.row];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *info = [self.mottoMutableArr objectAtIndex:indexPath.row];
    return [MottoSingleTableViewCell calculationCellHeightWithTitle:info];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.mottoTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [self.mottoMutableArr count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.mottoMutableArr  count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// 定义编辑样式
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

// 进入编辑模式，按下出现的编辑按钮后,进行删除操作
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSString *info = [self.mottoMutableArr objectAtIndex:indexPath.row];
        [self deleteWithMotto:info];
    }
}

// 修改编辑按钮文字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}


#pragma mark - Interface
-(void)sendRequestToGetInterface{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Motto_List requestParams:nil responseObjectClass:[MottoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            MottoModel *mottoModel = (MottoModel *)responseObject;
            [strongSelf.mottoMutableArr addObjectsFromArray:mottoModel.list];
            [strongSelf.mottoTableView reloadData];
        }
    }];
}

#pragma mark - Interface
-(void)deleteWithMotto:(NSString *)motto{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] mottoDeleteWithmotto:motto block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [StatusBarManager statusBarHidenWithText:@"删除成功"];
        NSInteger index = [self.mottoMutableArr indexOfObject:motto];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [strongSelf.mottoMutableArr removeObject:motto];
        [strongSelf.mottoTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }];
}


@end
