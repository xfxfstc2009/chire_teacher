//
//  MottoSingleTableViewCell.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "MottoSingleTableViewCell.h"

@interface MottoSingleTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation MottoSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
}

-(void)setTransferInfo:(NSString *)transferInfo{
    _transferInfo = transferInfo;
    self.titleLabel.text = transferInfo;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(7), kScreenBounds.size.width - 2 * LCFloat(11), titleSize.height);
}

+(CGFloat)calculationCellHeightWithTitle:(NSString *)title{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(7);
    CGSize titleSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += titleSize.height;
    cellHeight += LCFloat(7);
    
    return MAX(cellHeight, LCFloat(44));
    
    return cellHeight;
}

@end
