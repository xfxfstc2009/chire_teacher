//
//  MottoSingleTableViewCell.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface MottoSingleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferInfo;

+(CGFloat)calculationCellHeightWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
