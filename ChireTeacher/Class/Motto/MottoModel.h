//
//  MottoModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MottoModel : FetchModel

@property (nonatomic,strong)NSArray *list;

@end

NS_ASSUME_NONNULL_END
