//
//  NetworkAdapter+Motto.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Motto)

-(void)mottoDeleteWithmotto:(NSString *)motto block:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
