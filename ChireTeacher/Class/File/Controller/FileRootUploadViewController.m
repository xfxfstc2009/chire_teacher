//
//  FileRootUploadViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/7.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FileRootUploadViewController.h"
#import "FileFileTitleTableViewCell.h"
#import "FileRootUploadTypeViewController.h"
#import "NetworkAdapter+FileManager.h"
#import "AliOSSManager.h"

@interface FileRootUploadViewController ()<UITableViewDelegate,UITableViewDataSource>{
    FileUploadType selectedFileUploadType;                  /**< 文件上传类型*/
    UITextField *nameTextField;
}
@property (nonatomic,strong)UITableView *fileTableView;
@property (nonatomic,strong)NSMutableArray *fileMutableArr;
@end

@implementation FileRootUploadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self fileUploadManager];
}

#pragma makr - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加内容";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    selectedFileUploadType = FileUploadTypeNone;
    self.fileMutableArr = [NSMutableArray array];
    [self.fileMutableArr addObjectsFromArray:@[@[@"文件名称"],@[@"上传类别"],@[@"文件信息"],@[@"提交"]]];
}

#pragma mark - UITableView
-(void)createTableView{
    self.fileTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
    self.fileTableView.dataSource = self;
    self.fileTableView.delegate = self;
    [self.view addSubview:self.fileTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.fileMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.fileMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"文件名称" sourceArr:self.fileMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"文件名称";
        cellWithRowTwo.transferPlaceholeder = @"文件重命名";
        cellWithRowTwo.transferInputText = [AccountModel sharedAccountModel].ossFileModel.objcName;
        nameTextField = cellWithRowTwo.inputTextField;
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"上传类别" sourceArr:self.fileMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWNormalTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"文件类别";
        cellWithRowThr.transferHasArrow = YES;
        if (selectedFileUploadType == FileUploadTypeNone){
            cellWithRowThr.transferDesc = @"请选择文件上传地址";
        } else if (selectedFileUploadType == FileUploadTypeDownload){
            cellWithRowThr.transferDesc = @"学习资料";
        }
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"文件信息" sourceArr:self.fileMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        FileFileTitleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[FileFileTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferOSSFileModel = [AccountModel sharedAccountModel].ossFileModel;
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWButtonTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferTitle = @"立即提交";
        __weak typeof(self)weakSelf = self;
        [cellWithRowFour buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToFileUploadManager];
        }];
        return cellWithRowFour;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [FileFileTitleTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"上传类别" sourceArr:self.fileMutableArr]){
        FileRootUploadTypeViewController *fileVC = [[FileRootUploadTypeViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [fileVC actionSelectedClickBlock:^(FileUploadType type) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->selectedFileUploadType = type;
            NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"上传类别" sourceArr:strongSelf.fileMutableArr];
            NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"上传类别" sourceArr:strongSelf.fileMutableArr];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:row inSection:section];
            [strongSelf.fileTableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationNone];
        }];
        
        [self.navigationController pushViewController:fileVC animated:YES];
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    headerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(20));
    
    UILabel *titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    titleLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), headerView.size_height);
    if (section == 0){
        titleLabel.text = @"文件内容";
    }
    
    [headerView addSubview:titleLabel];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

#pragma mark - 文件上传
-(void)sendRequestToFileUploadManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fileUploadWithType:FileUploadTypeDownload name:nameTextField.text info:@"123" url:[AccountModel sharedAccountModel].ossFileModel.fileUrl ablum:@"123" block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"文件上传成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
    }];
}

-(void)fileUploadManager{
    [[AliOSSManager sharedOssManager] uploadFileWithType:OSSUploadTypeOther name:[AccountModel sharedAccountModel].ossFileModel.objcName obj:[AccountModel sharedAccountModel].ossFileModel block:^(NSString *fileUrl) {
        [AccountModel sharedAccountModel].ossFileModel.fileUrl = fileUrl;
        [PDHUD dismissManager];
    } progress:^(CGFloat progress) {
        NSString *progressStr = [NSString stringWithFormat:@"%.2f",progress];
//        [PDHUD showHUDProgress:progressStr diary:0];
    }];
}

@end
