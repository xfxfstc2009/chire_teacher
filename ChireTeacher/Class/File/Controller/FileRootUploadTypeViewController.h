//
//  FileRootUploadTypeViewController.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface FileRootUploadTypeViewController : AbstractViewController

-(void)actionSelectedClickBlock:(void(^)(FileUploadType type))block;

@end

NS_ASSUME_NONNULL_END
