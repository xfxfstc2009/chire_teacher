//
//  FileRootUploadTypeViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FileRootUploadTypeViewController.h"

static char actionSelectedClickBlockKey;
@interface FileRootUploadTypeViewController()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *fileTypeTableView;
@property (nonatomic,strong)NSArray *fileTypeArr;
@property (nonatomic,strong)NSMutableArray *selectedMutableArr;
@end

@implementation FileRootUploadTypeViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"上传位置选择";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.fileTypeArr = @[@"学习资料",@"考试试题"];
    self.selectedMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.fileTypeTableView){
        self.fileTypeTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.fileTypeTableView.dataSource = self;
        self.fileTypeTableView.delegate = self;
        [self.view addSubview:self.fileTypeTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.fileTypeArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWSelectedTableViewCell *selectedTableViewCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!selectedTableViewCell){
        selectedTableViewCell = [[GWSelectedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    selectedTableViewCell.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    selectedTableViewCell.transferTitle = [self.fileTypeArr objectAtIndex:indexPath.row];
    if ([self.selectedMutableArr containsObject:selectedTableViewCell.transferTitle]){
        [selectedTableViewCell setChecked:YES];
    } else {
        [selectedTableViewCell setChecked:NO];
    }
    
    return selectedTableViewCell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.fileTypeTableView) {
        if (indexPath.section == 0){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
            } else if ([indexPath row] == [self.fileTypeArr count] - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
            }
            if ([self.fileTypeArr count] == 1) {
                separatorType  = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *info = [self.fileTypeArr objectAtIndex:indexPath.row];
    if (self.selectedMutableArr.count){
        [self.selectedMutableArr removeAllObjects];
    }
    [self.selectedMutableArr addObject:info];
    
    void(^block)() = objc_getAssociatedObject(self, &actionSelectedClickBlockKey);
    
    FileUploadType type = FileUploadTypeNone;
    if ([info isEqualToString:@"学习资料"]){
        type = FileUploadTypeDownload;
    } else if ([info isEqualToString:@"考试试题"]){
        type = FileUploadTypeExam;
    }
    
    if (block){
        block(type);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(44);
}

-(void)actionSelectedClickBlock:(void(^)(FileUploadType type))block{
    objc_setAssociatedObject(self, &actionSelectedClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
