//
//  FileFileTitleTableViewCell.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/7.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FileFileTitleTableViewCell.h"

@interface FileFileTitleTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *imgLabel;
@end

@implementation FileFileTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createViw
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(60), LCFloat(60));
    [self addSubview:self.iconImgView];
    
    self.imgLabel = [GWViewTool createLabelFont:@"15" textColor:@"黑"];
    self.imgLabel.backgroundColor = [UIColor clearColor];
    self.imgLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(11), self.iconImgView.orgin_y, kScreenBounds.size.width - LCFloat(11) * 2 - CGRectGetMaxX(self.iconImgView.frame), [NSString contentofHeightWithFont:self.imgLabel.font]);
    [self addSubview:self.imgLabel];
}

-(void)setTransferOSSFileModel:(OSSSingleFileModel *)transferOSSFileModel{
    _transferOSSFileModel = transferOSSFileModel;
    
    self.iconImgView.image = [UIImage imageNamed:transferOSSFileModel.fileType];
    
    self.imgLabel.text = transferOSSFileModel.objcName;
    CGSize imgLabelSize = [self.imgLabel.text sizeWithCalcFont:self.imgLabel.font constrainedToSize:CGSizeMake(self.imgLabel.size_width, CGFLOAT_MAX)];
    self.imgLabel.size_height = imgLabelSize.height;
    if (imgLabelSize.height >= 2 * [NSString contentofHeightWithFont:self.imgLabel.font]){
        self.imgLabel.size_height = 2 * [NSString contentofHeightWithFont:self.imgLabel.font];
        self.imgLabel.numberOfLines = 2;
    } else {
        self.imgLabel.size_height = 1 * [NSString contentofHeightWithFont:self.imgLabel.font];
        self.imgLabel.numberOfLines = 1;
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(11) * 2 + LCFloat(60);
}

@end
