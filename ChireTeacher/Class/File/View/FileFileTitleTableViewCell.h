//
//  FileFileTitleTableViewCell.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/7.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "PDBaseTableViewCell.h"
#import "OSSSingleFileModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface FileFileTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)OSSSingleFileModel *transferOSSFileModel;

@end

NS_ASSUME_NONNULL_END
