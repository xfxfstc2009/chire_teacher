//
//  NetworkAdapter+FileManager.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter.h"


NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (FileManager)

-(void)fileUploadWithType:(FileUploadType)type name:(NSString *)name info:(NSString *)info url:(NSString *)url ablum:(NSString *)ablum block:(void(^)())block;

@end

NS_ASSUME_NONNULL_END
