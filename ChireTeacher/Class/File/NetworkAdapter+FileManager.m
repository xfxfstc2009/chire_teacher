//
//  NetworkAdapter+FileManager.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter+FileManager.h"



@implementation NetworkAdapter (FileManager)

// 文件上传
-(void)fileUploadWithType:(FileUploadType)type name:(NSString *)name info:(NSString *)info url:(NSString *)url ablum:(NSString *)ablum block:(void(^)())block{
    if (type == FileUploadTypeDownload){
        [self fileUploadWithName:name info:info url:url ablum:ablum block:block];
    }
}

#pragma mark - 上传下载内容
-(void)fileUploadWithName:(NSString *)name info:(NSString *)info url:(NSString *)url ablum:(NSString *)ablum block:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"name":name,@"info":info,@"url":url,@"ablum":ablum};
    [[NetworkAdapter sharedAdapter] fetchWithPath:download_Add requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (block){
                block();
            }
        }
    }];
}

@end
