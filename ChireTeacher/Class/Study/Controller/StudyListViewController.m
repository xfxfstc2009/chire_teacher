//
//  StudyListViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/17.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "StudyListViewController.h"
#import "StudyFileModel.h"
#import "FileRootUploadViewController.h"
@interface StudyListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *studyFileListTableView;
@property (nonatomic,strong)NSMutableArray *studyFileMutableArr;
@end

@implementation StudyListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetExamFileList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"学习资料";
    [self rightBarButtonWithTitle:@"12" barNorImage:nil barHltImage:nil action:^{
        FileRootUploadViewController *fileRootVC = [[FileRootUploadViewController alloc]init];
        [self.navigationController pushViewController:fileRootVC animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.studyFileMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.studyFileListTableView){
        self.studyFileListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.studyFileListTableView.dataSource = self;
        self.studyFileListTableView.delegate = self;
        [self.view addSubview:self.studyFileListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.studyFileMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    StudyFileModel *studyFileModel = [self.studyFileMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferCellHeight = 44;
    cellWithRowOne.transferTitle = studyFileModel.name;
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    StudyFileModel *studyFileModel = [self.studyFileMutableArr objectAtIndex:indexPath.row];
    
    PDWebViewController *webViewController = [[PDWebViewController alloc]init];
    NSString *newUrl = [NSString stringWithFormat:@"%@%@",aliyunOSS_BaseURL,studyFileModel.url];
    [webViewController webDirectedWebUrl:newUrl];
    [self.navigationController pushViewController:webViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Interface
-(void)sendRequestToGetExamFileList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Study_File_List requestParams:nil responseObjectClass:[StudyFileListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            StudyFileListModel *studyListModel = (StudyFileListModel *)responseObject;
            [strongSelf.studyFileMutableArr addObjectsFromArray:studyListModel.list];
            [strongSelf.studyFileListTableView reloadData];
        }
    }];
}




#pragma mark - sendRequestInfo
-(void)sendRequestToInfo{
    OSSSingleFileModel *fileModel = [[OSSSingleFileModel alloc]init];

    fileModel.objcName = @"哈哈哈smart.pdf";
    [[AliOSSManager sharedOssManager] uploadFileWithType:OSSUploadTypeOther name:@"123" obj:fileModel block:^(NSString *fileUrl) {
        NSLog(@"%@",fileUrl);
    } progress:^(CGFloat progress) {
        NSLog(@"%.2f",progress);
    }];
}
@end
