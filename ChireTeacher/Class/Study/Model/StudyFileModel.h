//
//  StudyFileModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/17.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol StudyFileModel <NSObject>

@end

@interface StudyFileModel : FetchModel

@property (nonatomic,copy)NSString *ablum;
@property (nonatomic,copy)NSString *datetime;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *info;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *url;

@end

@interface StudyFileListModel : FetchModel

@property (nonatomic,strong)NSArray<StudyFileModel> *list;

@end


NS_ASSUME_NONNULL_END
