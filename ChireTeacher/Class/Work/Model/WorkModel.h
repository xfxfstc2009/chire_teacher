//
//  WorkModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,WorkPushType) {
    WorkPushTypeClass = 1,
    WorkPushTypeStudent = 2,
};

@protocol WorkModel <NSObject>

@end

@interface WorkModel : FetchModel

@property (nonatomic,copy)NSString *date;
@property (nonatomic,assign)NSInteger hasPush;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)WorkPushType pushType;
@property (nonatomic,copy)NSString *pushUser;
@property (nonatomic,strong)NSArray *workList;
@property (nonatomic,copy)NSString *workName;

@end


@interface WorkListModel : FetchModel

@property (nonatomic,strong)NSArray<WorkModel> *list;

@end

NS_ASSUME_NONNULL_END
