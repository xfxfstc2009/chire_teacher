//
//  NetworkAdapter+Work.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter.h"
#import "WorkModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Work)

-(void)addWork:(NSArray *)workList pushType:(WorkPushType)type pushUser:(NSString *)pushUser hasPush:(BOOL)hasPush block:(void(^)(BOOL isSuccessed))block;
@end

NS_ASSUME_NONNULL_END
