//
//  WorkPushUserChooseViewController.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/14.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AbstractViewController.h"
#import "NetworkAdapter+Work.h"
#import "NetworkAdapter+Class.h"
#import "NetworkAdapter+Student.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorkPushUserChooseViewController : AbstractViewController

-(void)actionAddWorkUserListBlock:(void(^)(WorkPushType type,NSArray *infoArr))block;

@end

NS_ASSUME_NONNULL_END
