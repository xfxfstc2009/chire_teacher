//
//  WorkRootViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "WorkRootViewController.h"
#import "WorkModel.h"
#import "WorkAddViewController.h"

@interface WorkRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *workListTableView;
@property (nonatomic,strong)NSMutableArray *workListMutableArr;
@end

@implementation WorkRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"作业";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"添加作业" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        WorkAddViewController *workAddVC = [[WorkAddViewController alloc]init];
        [strongSelf.navigationController pushViewController:workAddVC animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.workListMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.workListTableView){
        self.workListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.workListTableView.dataSource = self;
        self.workListTableView.delegate = self;
        [self.view addSubview:self.workListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.workListMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = 44;
    WorkModel *workModel = [self.workListMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = workModel.workName;
    return cellWithRowOne;
}

#pragma mark - Interface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Work_List requestParams:nil responseObjectClass:[WorkListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            WorkListModel *workListModel = (WorkListModel *)responseObject;
            [strongSelf.workListMutableArr addObjectsFromArray:workListModel.list];
            [strongSelf.workListTableView reloadData];
        }
    }];
}

@end
