//
//  WorkAddViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "WorkAddViewController.h"
#import "NetworkAdapter+Work.h"
#import "WorkAddSingleWorkViewController.h"
#import "WorkPushUserChooseViewController.h"

@interface WorkAddViewController ()<UITableViewDelegate,UITableViewDataSource>{
    BOOL hasPush;
    NSMutableArray *selectedMutableArr;
    WorkPushType workPushType;
}
@property (nonatomic,strong)UITableView *workTaleView;
@property (nonatomic,strong)NSMutableArray *workMutableArr;
@property (nonatomic,strong)NSMutableArray *workListMutableArr;
@end

@implementation WorkAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加作业";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.workMutableArr = [NSMutableArray array];
    self.workListMutableArr = [NSMutableArray array];
    [self.workListMutableArr addObject:@"添加按钮"];
    [self.workMutableArr addObject:self.workListMutableArr];
    [self.workMutableArr addObject:@[@"推送人",@"是否推送"]];
    [self.workMutableArr addObject:@[@"按钮"]];
    hasPush = YES;
    selectedMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.workTaleView){
        self.workTaleView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.workTaleView.dataSource = self;
        self.workTaleView.delegate = self;
        [self.view addSubview:self.workTaleView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.workMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.workMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.workMutableArr]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        GWButtonTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        cellWithRowFiv.transferTitle = @"提交";
        __weak typeof(self)weakSelf = self;
        [cellWithRowFiv buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToPushInfo];
        }];
        return cellWithRowFiv;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"推送人" sourceArr:self.workMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"推送类型" sourceArr:self.workMutableArr] || indexPath.row == [self cellIndexPathRowWithcellData:@"推送人" sourceArr:self.workMutableArr]){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowOne){
                cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferTitle = [[self.workMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            cellWithRowOne.transferHasArrow = YES;
            
            
            NSString *dymicStr = @"";
            if (workPushType == WorkPushTypeClass){     // 班级
                for (int i = 0 ; i < selectedMutableArr.count;i++){
                    ClassModel *classModel = [selectedMutableArr objectAtIndex:i];
                    if (i == selectedMutableArr.count - 1){
                        dymicStr = [dymicStr stringByAppendingString:[NSString stringWithFormat:@"%@,",classModel.name]];
                    } else {
                        dymicStr = [dymicStr stringByAppendingString:[NSString stringWithFormat:@"%@",classModel.name]];
                    }
                }
            } else if (workPushType == WorkPushTypeStudent){    // 学生
                for (int i = 0 ; i < selectedMutableArr.count;i++){
                    StudentModel *studentModel = [selectedMutableArr objectAtIndex:i];
                    if (i == selectedMutableArr.count - 1){
                       dymicStr = [dymicStr stringByAppendingString:[NSString stringWithFormat:@"%@,",studentModel.name]];
                    } else {
                       dymicStr = [dymicStr stringByAppendingString:[NSString stringWithFormat:@"%@",studentModel.name]];
                    }
                }
            }
            
            cellWithRowOne.transferDesc = dymicStr;
            return cellWithRowOne;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"是否推送" sourceArr:self.workMutableArr]){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            GWSwitchTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[GWSwitchTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            }
            cellWithRowFour.transferCellHeight = cellHeight;
            cellWithRowFour.transferInfo = @"是否推送";
            cellWithRowFour.isOn = YES;
            __weak typeof(self)weakSelf = self;
            [cellWithRowFour actionClickWIthSwitch:^(BOOL switchStatus) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                cellWithRowFour.isOn = switchStatus;
                strongSelf->hasPush = switchStatus;
            }];
            return cellWithRowFour;
        } else {
            static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
            GWNormalTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
            if (!cellWithRowOther){
                cellWithRowOther = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            }
            cellWithRowOther.transferCellHeight = cellHeight;
            cellWithRowOther.transferTitle = @"123";
            
            return cellWithRowOther;
        }
    } else {
        NSInteger section = [self cellIndexPathSectionWithcellData:@"添加按钮" sourceArr:self.workMutableArr];
        NSInteger row =  [self cellIndexPathRowWithcellData:@"添加按钮" sourceArr:self.workMutableArr];
        if (indexPath.section == section){
            if (indexPath.row == row){
                static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
                GWButtonTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
                if (!cellWithRowOne){
                    cellWithRowOne = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                }
                cellWithRowOne.transferCellHeight = cellHeight;
                cellWithRowOne.transferTitle = @"添加一条作业";
                __weak typeof(self)weakSelf = self;
                [cellWithRowOne buttonClickManager:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf addWorkInfo];
                }];
                return cellWithRowOne;
            } else {
                static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
                GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
                if (!cellWithRowTwo){
                    cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                }
                cellWithRowTwo.transferCellHeight = cellHeight;
                cellWithRowTwo.transferTitle = [[self.workMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
                
                return cellWithRowTwo;
            }
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"推送人" sourceArr:self.workMutableArr]){
        WorkPushUserChooseViewController *workPushVC = [[WorkPushUserChooseViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [workPushVC actionAddWorkUserListBlock:^(WorkPushType type, NSArray * _Nonnull infoArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->workPushType = type;
            [strongSelf->selectedMutableArr removeAllObjects];
            [strongSelf->selectedMutableArr addObjectsFromArray:infoArr];
            
            NSInteger row = [self cellIndexPathRowWithcellData:@"推送人" sourceArr:self.workMutableArr];
            NSInteger section = [self cellIndexPathSectionWithcellData:@"推送人" sourceArr:self.workMutableArr];
            NSIndexPath *pushIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
            [tableView reloadRowsAtIndexPaths:@[pushIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
        [self.navigationController pushViewController:workPushVC animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.workMutableArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"推送人" sourceArr:self.workMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"推送类型" sourceArr:self.workMutableArr] || indexPath.row == [self cellIndexPathRowWithcellData:@"推送人" sourceArr:self.workMutableArr]){
            return [GWNormalTableViewCell calculationCellHeight];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"是否推送" sourceArr:self.workMutableArr]){
            return [GWSwitchTableViewCell calculationCellHeight];
        } else {
            return [GWNormalTableViewCell calculationCellHeight];
        }
    } else {
        NSInteger row =  [self cellIndexPathRowWithcellData:@"添加按钮" sourceArr:self.workMutableArr];
        if (indexPath.row == row){
            return [GWButtonTableViewCell calculationCellHeight];
        } else {
            return [GWNormalTableViewCell calculationCellHeight];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.workTaleView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.workMutableArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.workMutableArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


#pragma mark - addWorkInfo
-(void)addWorkInfo{
    WorkAddSingleWorkViewController *workSingleController = [[WorkAddSingleWorkViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [workSingleController actionClickWithAddWorkBlock:^(NSString * _Nonnull info) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger index = [strongSelf cellIndexPathRowWithcellData:@"添加按钮" sourceArr:self.workMutableArr];
        [strongSelf.workListMutableArr insertObject:info atIndex:index];
        
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
        [strongSelf.workTaleView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }];
    [self.navigationController pushViewController:workSingleController animated:YES];
}

#pragma mark - Interface
// 1.
-(void)sendRequestToPushInfo{
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < selectedMutableArr.count;i++){
        NSString *userId;
        if (workPushType == WorkPushTypeClass){
            ClassModel *classModel = [selectedMutableArr objectAtIndex:i];
            userId = classModel.ID;
        } else if (workPushType == WorkPushTypeStudent){
            StudentModel *studentModel = [selectedMutableArr objectAtIndex:i];
            userId = studentModel.ID;
        }
        
        NSMutableArray *workListTempMutableArr = [NSMutableArray array];
        [workListTempMutableArr addObjectsFromArray:[self.workListMutableArr copy]];
        [workListTempMutableArr removeObject:@"添加按钮"];
        [[NetworkAdapter sharedAdapter] addWork:workListTempMutableArr pushType:workPushType pushUser:userId hasPush:hasPush block:^(BOOL isSuccessed) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (i == strongSelf-> selectedMutableArr.count - 1){
                [[UIAlertView alertViewWithTitle:@"提交成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
            }
        }];
    }
}

@end
