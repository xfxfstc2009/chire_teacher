//
//  WorkAddSingleWorkViewController.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/14.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AbstractViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WorkAddSingleWorkViewController : AbstractViewController

-(void)actionClickWithAddWorkBlock:(void(^)(NSString *info))block;

@end

NS_ASSUME_NONNULL_END
