//
//  WorkAddSingleWorkViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/14.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "WorkAddSingleWorkViewController.h"

static char actionClickWithAddWorkBlockKey;
@interface WorkAddSingleWorkViewController ()<UITableViewDelegate,UITableViewDataSource>{
    UITextField *inputTextField;
}
@property (nonatomic,strong)UITableView *workTableView;
@property (nonatomic,strong)NSArray *workArr;
@end

@implementation WorkAddSingleWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
    
}

#pragma mark pageSetting
-(void)pageSetting{
    self.barMainTitle = @"添加作业";
}

#pragma mark - arrayWithIit
-(void)arrayWithInit{
    self.workArr = @[@[@"作业"],@[@"按钮"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.workTableView){
        self.workTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.workTableView.dataSource = self;
        self.workTableView.delegate = self;
        [self.view addSubview:self.workTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.workArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.workArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferTitle = @"作业";
        cellWithRowOne.transferPlaceholeder = @"请添加一条作业";
    
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;

        }];
        inputTextField = cellWithRowOne.inputTextField;
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"添加作业";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (!strongSelf->inputTextField.text.length){
                [StatusBarManager statusBarHidenWithText:@"请输入作业信息"];
                return;
            } else {
                void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &actionClickWithAddWorkBlockKey);
                if (block){
                    block(strongSelf->inputTextField.text);
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }
        }];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [GWInputTextFieldTableViewCell calculationCellHeight];
    } else {
        return [GWButtonTableViewCell calculationCellHeight];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(void)actionClickWithAddWorkBlock:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &actionClickWithAddWorkBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
