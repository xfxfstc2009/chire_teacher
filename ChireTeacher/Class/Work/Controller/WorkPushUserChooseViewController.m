//
//  WorkPushUserChooseViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/14.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "WorkPushUserChooseViewController.h"


static char actionAddWorkUserListBlockKey;
@interface WorkPushUserChooseViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UITableView *userTableView;
@property (nonatomic,strong)NSMutableArray *userMutableArr;
@property (nonatomic,strong)UITableView *classTableView;
@property (nonatomic,strong)NSMutableArray *classMutableArr;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSMutableArray *userSelectedMutableArr;
@property (nonatomic,strong)NSMutableArray *classSelectedMutableArr;


@end

@implementation WorkPushUserChooseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSegmentList];
    [self createMainScrollView];
    [self createTableView];
    [self createInterfaceGetWorkPushClassList];
    [self createInterfaceGetStudentList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"选择推送对象";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(WorkPushType type,NSArray *infoArr) = objc_getAssociatedObject(strongSelf, &actionAddWorkUserListBlockKey);
        if (block){
            if (strongSelf.userSelectedMutableArr.count){
                block(WorkPushTypeStudent,strongSelf.userSelectedMutableArr);
            } else if (strongSelf.classSelectedMutableArr.count){
                block(WorkPushTypeClass,strongSelf.classSelectedMutableArr);
            }
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - CreateSegmentList
-(void)createSegmentList{
    if (!self.segmentList){
        __weak typeof(self)weakSelf = self;
        self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"班级",@"学生"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.mainScrollView setContentOffset:CGPointMake(index * self.mainScrollView.size_width, 0) animated:YES];
        }];
        self.segmentList.backgroundColor = [UIColor whiteColor];
        self.segmentList.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44));
        [self.view addSubview:self.segmentList];
    }
}

#pragma mark - Main
-(void)createMainScrollView{
    if (!self.mainScrollView){
        __weak typeof(self)weakSelf = self;
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger page = scrollView.contentOffset.x / kScreenBounds.size.width;
            [strongSelf.segmentList setSelectedButtonIndex:page animated:YES];
        }];
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.segmentList.frame));
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2, self.mainScrollView.size_height);
        [self.view addSubview:self.mainScrollView];
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.userMutableArr = [NSMutableArray array];
    self.classMutableArr = [NSMutableArray array];
    self.classSelectedMutableArr = [NSMutableArray array];
    self.userSelectedMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.classTableView){
        self.classTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.classTableView.dataSource = self;
        self.classTableView.delegate = self;
        [self.mainScrollView addSubview:self.classTableView];
    }
    
    if (!self.userTableView){
        self.userTableView = [GWViewTool gwCreateTableViewRect:self.mainScrollView.bounds];
        self.userTableView.orgin_x = kScreenBounds.size.width;
        self.userTableView.delegate = self;
        self.userTableView.dataSource = self;
        [self.mainScrollView addSubview:self.userTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.classTableView){
        return self.classMutableArr.count;
    } else if (tableView == self.userTableView){
        return self.userMutableArr.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWSelectedTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWSelectedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    if (tableView == self.classTableView){
        ClassModel *classModel = [self.classMutableArr objectAtIndex:indexPath.row];
        cellWithRowOne.transferTitle = classModel.name;
        if ([self.classSelectedMutableArr containsObject:classModel]){
            [cellWithRowOne setChecked:YES];
        } else {
            [cellWithRowOne setChecked:NO];
        }
    } else if (tableView == self.userTableView){
        StudentModel *studentModel = [self.userMutableArr objectAtIndex:indexPath.row];
        cellWithRowOne.transferTitle = studentModel.name;
        if ([self.userSelectedMutableArr containsObject:studentModel]){
            [cellWithRowOne setChecked:YES];
        } else {
            [cellWithRowOne setChecked:NO];
        }
    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    GWSelectedTableViewCell *cell = (GWSelectedTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    if (tableView == self.userTableView){
        
        StudentModel *studentModel = [self.userMutableArr objectAtIndex:indexPath.row];
        if ([self.userSelectedMutableArr containsObject:studentModel]){
            [self.userSelectedMutableArr removeObject:studentModel];
            [cell setChecked:NO];
        } else {
            [self.userSelectedMutableArr addObject:studentModel];
            [cell setChecked:YES];
        }
        
        // 删除班级
        [self.classSelectedMutableArr removeAllObjects];
        for (GWSelectedTableViewCell *cell in self.classTableView.visibleCells){
            [cell setChecked:NO];
        }

    } else if (tableView == self.classTableView){
        ClassModel *classModel = [self.classMutableArr objectAtIndex:indexPath.row];
        if ([self.classSelectedMutableArr containsObject:classModel]){
            [self.classSelectedMutableArr removeObject:classModel];
            [cell setChecked:NO];
        } else {
            [self.classSelectedMutableArr addObject:classModel];
            [cell setChecked:YES];
        }
        
        // 删除学生
        [self.userSelectedMutableArr removeAllObjects];
        for (GWSelectedTableViewCell *cell in self.userTableView.visibleCells){
            [cell setChecked:NO];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [GWSelectedTableViewCell calculationCellHeight];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *infoArr;
    
    if (tableView == self.userTableView) {
        infoArr = self.userMutableArr;
    } else if (tableView == self.classTableView){
        infoArr = self.classMutableArr;
    }
    
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType  = SeparatorTypeHead;
    } else if ([indexPath row] == [infoArr count] - 1) {
        separatorType  = SeparatorTypeBottom;
    } else {
        separatorType  = SeparatorTypeMiddle;
    }
    if ([infoArr  count] == 1) {
        separatorType  = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithType:separatorType];
}


#pragma mark - Interface
-(void)createInterfaceGetWorkPushClassList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] classListAllBlock:^(ClassListModel * _Nonnull classListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.classMutableArr removeAllObjects];
        [strongSelf.classMutableArr addObjectsFromArray:classListModel.list];
        [strongSelf.classTableView reloadData];
    }];
}

// 获取学生列表
-(void)createInterfaceGetStudentList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] getAllStudentListWithBlock:^(StudentListModel * _Nonnull studentList) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.userMutableArr removeAllObjects];
        [strongSelf.userMutableArr addObjectsFromArray:studentList.list];
        [strongSelf.userTableView reloadData];
    }];
}

-(void)actionAddWorkUserListBlock:(void(^)(WorkPushType type,NSArray *infoArr))block{
    objc_setAssociatedObject(self, &actionAddWorkUserListBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
