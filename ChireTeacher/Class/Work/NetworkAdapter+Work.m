//
//  NetworkAdapter+Work.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/12.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter+Work.h"

@implementation NetworkAdapter (Work)

-(void)addWork:(NSArray *)workList pushType:(WorkPushType)type pushUser:(NSString *)pushUser hasPush:(BOOL)hasPush block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSString *workString = @"";
    for (int i = 0 ; i < workList.count ;i++){
        NSString *workInfo = [workList objectAtIndex:i];
        if (i == workList.count - 1){
            workString = [workString stringByAppendingString:[NSString stringWithFormat:@"%li.%@",(long)i + 1,workInfo]];
        } else {
            workString = [workString stringByAppendingString:[NSString stringWithFormat:@"%li.%@,",(long)i + 1,workInfo]];
        }
    }
    
    NSDictionary *params = @{@"workInfo":workString,@"pushType":@(type),@"pushUser":pushUser,@"hasPush":@(hasPush)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:Work_Add requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)weakSelf = weakSelf;
        if (isSucceeded){
            if (block){
                block(YES);
            }
        }
    }];
    
}

@end
