//
//  SchoolAddViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/7.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "SchoolAddViewController.h"

@interface SchoolAddViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *schoolTableView;
@property (nonatomic,strong)NSArray *schoolArr;


@end

@implementation SchoolAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

-(void)pageSetting{
    self.barMainTitle = @"学校地址添加";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.schoolArr = @[@[@"学校名称"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.schoolTableView){
        self.schoolTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.schoolTableView.dataSource = self;
        self.schoolTableView.delegate = self;
        [self.view addSubview:self.schoolTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.schoolArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.schoolArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - UItableView
-(void)sendRequestToGetInfoMnager{
    __weak typeof(self)weakSelf = self;
    
}
@end
