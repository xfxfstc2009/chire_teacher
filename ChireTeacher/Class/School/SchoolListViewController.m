//
//  SchoolListViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "SchoolListViewController.h"
#import "SchoolModel.h"
#import "SchoolAddViewController.h"

@interface SchoolListViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *schoolTableView;
@property (nonatomic,strong)NSMutableArray *schoolMutableArr;
@end

@implementation SchoolListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetSchoolList];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的学校";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"添加" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        SchoolAddViewController *schoolAddVC = [[SchoolAddViewController alloc]init];
        [strongSelf.navigationController pushViewController:schoolAddVC animated:YES];
    }];
}

-(void)arrayWithInit{
    self.schoolMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.schoolTableView){
        self.schoolTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.schoolTableView.dataSource = self;
        self.schoolTableView.delegate = self;
        [self.view addSubview:self.schoolTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.schoolMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferCellHeight = cellHeight;
    SchoolModel *schoolModel = [self.schoolMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = schoolModel.school;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

#pragma mark - Interface
-(void)sendRequestToGetSchoolList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:School_List requestParams:nil responseObjectClass:[SchoolListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            SchoolListModel *listModel = (SchoolListModel *)responseObject;
            [strongSelf.schoolMutableArr addObjectsFromArray:listModel.list];
            [strongSelf.schoolTableView reloadData];
        }
    }];
}
@end
