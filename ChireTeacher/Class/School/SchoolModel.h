//
//  SchoolModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/11.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol SchoolModel <NSObject>

@end

@interface SchoolModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *lat;
@property (nonatomic,copy)NSString *lon;
@property (nonatomic,copy)NSString *school;

@end



@interface SchoolListModel : FetchModel

@property (nonatomic,strong)NSArray<SchoolModel> *list;
@end


NS_ASSUME_NONNULL_END
