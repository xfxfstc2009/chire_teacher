//
//  PDLaungchViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/11/12.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDLaungchViewController.h"
#import "PDAppleShenheModel.h"
#import "TextAnimation.h"

@interface PDLaungchViewController ()
@property (nonatomic,strong)PDImageView *launchImageView;
@property (nonatomic,strong)PDImageView *logoImageView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *moonLabel;
@property (nonatomic,strong)UILabel *contentLabel;
@property (nonatomic,strong)UITapGestureRecognizer *tapGesture;
@property (nonatomic,strong)TextAnimation *textAnimationView;           /**< 文字动画*/
@property (nonatomic,strong)LOTAnimationView *animation;
@end

@implementation PDLaungchViewController

-(void)dealloc{
    NSLog(@"释放");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.launchImageView];
    [self.view bringSubviewToFront:self.launchImageView];
    [self createView];
    __weak typeof(self)waekSelf = self;
    [waekSelf sendRequestGetLaunchImageInfo];
}

#pragma mark - createView
-(void)createView{
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.textColor = [UIColor whiteColor];
    self.numberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:120];
    self.numberLabel.autoresizesSubviews = YES;
    self.numberLabel.text = [NSDate getCurrentTimeWithDay];
    self.numberLabel.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:self.numberLabel];
    
    // Moon
    self.moonLabel = [[UILabel alloc] init];
    self.moonLabel.textColor = [UIColor whiteColor];
    self.moonLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:30];
    
    NSString *moon =  [NSDate getCurrentTimeWithMoon];
    NSString *realMoon = @"";
    if ([moon isEqualToString:@"1"]){
        realMoon = @"Jan.";
    } else if ([moon isEqualToString:@"2"]){
        realMoon = @"Feb.";
    } else if ([moon isEqualToString:@"3"]){
        realMoon = @"Mar.";
    } else if ([moon isEqualToString:@"4"]){
        realMoon = @"Apr.";
    } else if ([moon isEqualToString:@"5"]){
        realMoon = @"May.";
    } else if ([moon isEqualToString:@"6"]){
        realMoon = @"Jun.";
    } else if ([moon isEqualToString:@"7"]){
        realMoon = @"Jul.";
    } else if ([moon isEqualToString:@"8"]){
        realMoon = @"Aug.";
    } else if ([moon isEqualToString:@"9"]){
        realMoon = @"Sep.";
    } else if ([moon isEqualToString:@"10"]){
        realMoon = @"Oct.";
    } else if ([moon isEqualToString:@"11"]){
        realMoon = @"Nov.";
    } else if ([moon isEqualToString:@"12"]){
        realMoon = @"Dec.";
    }
    
    self.moonLabel.text = realMoon;
    [self.view addSubview:self.moonLabel];
    
    // Moon
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.textColor = [UIColor whiteColor];
    self.contentLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20];
    self.contentLabel.numberOfLines = 0;
    [self.view addSubview:self.contentLabel];
    
    // 4. 创建手势
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(animationStopTapManager)];
    [self.view addGestureRecognizer:self.tapGesture];


}

-(void)sendRequestGetLaunchImageInfo{
    __weak typeof(self)weakSelf = self;
    [self.textAnimationView showAnimationWithFinishBlock:^{
        [self dismissAnimationManager];
    }];
    return;

//    [[NetworkAdapter sharedAdapter] fetchWithPath:@"" requestParams:nil responseObjectClass:[PDAppleShenheModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf =weakSelf;
//        if (isSucceeded){
//            PDAppleShenheModel *shenheModel = (PDAppleShenheModel *)responseObject;
//            if ([shenheModel.name isEqualToString:@"smart"]){
//                [AccountModel sharedAccountModel].isShenhe = YES;
//            } else {
//                [AccountModel sharedAccountModel].isShenhe = NO;
//            }
//
//            // debug
//
//            [AccountModel sharedAccountModel].isShenhe = NO;
////            shenheModel.show_type= 2;
//
//            [AccountModel sharedAccountModel].login_delegate = shenheModel.login_delegate;
//            [strongSelf shenheManager];
//
//            if (shenheModel.show_type == 2){
//                [strongSelf.textAnimationView showAnimationWithFinishBlock:^{
//                    [strongSelf dismissAnimationManager];
//                }];
//            } else {
//                [strongSelf.launchImageView uploadImageWithURL:shenheModel.lungch_img placeholder:nil callback:^(UIImage *image) {
//                    [strongSelf.view addSubview:strongSelf.logoImageView];
//
//                    // content
//                    strongSelf.contentLabel.text = shenheModel.lungch_content;
//                    CGSize contentSize = [strongSelf.contentLabel.text sizeWithCalcFont:strongSelf.contentLabel.font constrainedToSize:CGSizeMake((kScreenBounds.size.width - 2 * LCFloat(11)), CGFLOAT_MAX)];
//                    strongSelf.contentLabel.frame = CGRectMake(LCFloat(11), kScreenBounds.size.height - LCFloat(30) - contentSize.height, (kScreenBounds.size.width - 2 * LCFloat(11)), contentSize.height);
//
//                    // day
//                    CGSize numberSize = [strongSelf.numberLabel.text sizeWithCalcFont:strongSelf.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 100)];
//                    self.numberLabel.frame = CGRectMake(LCFloat(11), strongSelf.contentLabel.orgin_y - LCFloat(25) - 100, numberSize.width, 100);
//
//                    // Moon
//                    CGSize moonSize = [strongSelf.moonLabel.text sizeWithCalcFont:strongSelf.moonLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 100)];
//                    strongSelf.moonLabel.frame = CGRectMake(CGRectGetMaxX(strongSelf.numberLabel.frame), strongSelf.numberLabel.orgin_y + (strongSelf.numberLabel.size_height - [NSString contentofHeightWithFont:strongSelf.moonLabel.font]), moonSize.width, [NSString contentofHeightWithFont:strongSelf.moonLabel.font]);
//
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                        [UIView animateWithDuration:3 animations:^{
//                            strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
//                            strongSelf.launchImageView.alpha = 0;
//                            strongSelf.view.alpha = 0;
//                        } completion:^(BOOL finished) {
//                            [strongSelf.view removeFromSuperview];
//                        }];
//                    });
//                }];
//            }
//        } else {
//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                [UIView animateWithDuration:3 animations:^{
//                    strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
//                    strongSelf.launchImageView.alpha = 0;
//                    strongSelf.view.alpha = 0;
//                } completion:^(BOOL finished) {
//                    [strongSelf.view removeFromSuperview];
//                }];
//            });
//        }
//    }];
}


#pragma mark - getter and setter

- (PDImageView *)launchImageView{
    
    if (_launchImageView == nil) {
        _launchImageView = [[PDImageView alloc] initWithFrame:kScreenBounds];
        _launchImageView.image = [UIImage imageNamed:@"bg1.jpg"];
    }
    
    return _launchImageView;
}

- (PDImageView *)logoImageView{
    
    if (_logoImageView == nil) {
        _logoImageView = [[PDImageView alloc] initWithFrame:CGRectMake(95, 458, 128, 100)];
        _logoImageView.image = [UIImage imageNamed:@"bg1.jpg"];
    }
    
    return _logoImageView;
}

#pragma mark - 手势点击页面消失
-(void)animationStopTapManager{
    [self dismissAnimationManager];
}


#pragma mark - dismissManager
-(void)dismissAnimationManager{
    if (_textAnimationView.hasDismiss == YES){
        return;
    }
    _textAnimationView.hasDismiss = YES;
    
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdDeflateTransitionFromView:self.view toView:keywindow.rootViewController.view originalPoint:keywindow.center duration:.9f completion:^{
        
    }];
}

-(TextAnimation *)textAnimationView{
    if (!_textAnimationView){
        CGRect animationViewRect = CGRectMake(0, 200, kScreenBounds.size.width, 80);
        _textAnimationView = [[TextAnimation alloc]initWithFrame:animationViewRect title:@"炽热教育"];
        _textAnimationView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:_textAnimationView];
        // 重置frame
        _textAnimationView.frame = CGRectMake((kScreenBounds.size.width - _textAnimationView.size_width) / 2.,kScreenBounds.size.height / 2. - LCFloat(140),_textAnimationView.size_width,_textAnimationView.size_height);
    }
    _textAnimationView.hasDismiss = NO;


    self.animation = [LOTAnimationView animationNamed:@"deer"];
    self.animation.frame = CGRectMake(0, CGRectGetMaxY(_textAnimationView.frame) + LCFloat(100), kScreenBounds.size.width, 80);
    [self.view addSubview:self.animation];
    self.animation.loopAnimation = NO;
    [self.animation playWithCompletion:NULL];
    return _textAnimationView;
}


-(void)shenheManager{
    if ([AccountModel sharedAccountModel].isShenhe){
//        [[PDMainTabbarViewController sharedController] shenheManager];
    }
}


@end
