//
//  PDAppleShenheModel.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/31.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface PDAppleShenheModel : FetchModel

@property (nonatomic,assign)BOOL enable;

@property (nonatomic,copy)NSString *appName;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *lungch_content;
@property (nonatomic,copy)NSString *lungch_img;
@property (nonatomic,assign)NSInteger show_type;
@property (nonatomic,copy)NSString *login_delegate;

@end
