//
//  HomeRootViewController.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/10.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "HomeRootViewController.h"
#import "ViewController.h"
#import "NetworkAdapter+Home.h"

#import "HomeLoopTableViewCell.h"
#import "MottoModel.h"

@interface HomeRootViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *homeTableView;
@property (nonatomic,strong)NSMutableArray *homeRootMutableArr;
@property (nonatomic,strong)NSMutableArray *bannerMutableArr;
@property (nonatomic,strong)NSMutableArray *mottoMutableArr;

@end

@implementation HomeRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetBannerInfo];
    [self getMotto];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"首页";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.bannerMutableArr = [NSMutableArray array];
    self.homeRootMutableArr = [NSMutableArray array];
    self.mottoMutableArr = [NSMutableArray array];
    [self.homeRootMutableArr addObjectsFromArray:@[@[@"banner"],@[@"箴言"],@[@"学生标题",@"学生详情"]]];
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.homeTableView){
        self.homeTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.homeTableView.dataSource = self;
        self.homeTableView.delegate = self;
        [self.view addSubview:self.homeTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.homeRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.homeRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.homeRootMutableArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWBannerTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWBannerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferImgArr = self.bannerMutableArr;

        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"箴言" sourceArr:self.homeRootMutableArr]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        HomeLoopTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[HomeLoopTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        cellWithRowTwo.transferMottoArr = self.mottoMutableArr;
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"学生标题" sourceArr:self.homeRootMutableArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWLineTitleTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWLineTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"炽热教育";
        
        return cellWithRowThr;
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        GWNormalTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        }
        return cellWithRowOther;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [GWBannerTableViewCell calculationCellHeight];
    } else {
        return [HomeLoopTableViewCell calculationCellHeight];
    }
    
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"banner" sourceArr:self.homeRootMutableArr] || section == [self cellIndexPathSectionWithcellData:@"箴言" sourceArr:self.homeRootMutableArr]){
        return 0;
    } else {
        return LCFloat(10);
    }
}

#pragma mark - Interface
-(void)sendRequestToGetBannerInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeSendRequestToGetHomeBannerBlock:^(HomeBannerListModel * _Nonnull list) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.bannerMutableArr addObjectsFromArray:list.list];
        [strongSelf.homeTableView reloadData];
    }];
}

-(void)getMotto{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:Motto_List requestParams:nil responseObjectClass:[MottoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            MottoModel *mottoList = (MottoModel *)responseObject;
            [strongSelf.mottoMutableArr addObjectsFromArray:mottoList.list];
            [strongSelf.homeTableView reloadData];
        }
    }];
}
@end
