//
//  HomeBannerModel.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/21.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol HomeBannerModel <NSObject>

@end

@interface HomeBannerModel : FetchModel

@property (nonatomic,copy)NSString *datetime;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,copy)NSString *url;

@end

@interface HomeBannerListModel : FetchModel

@property (nonatomic,strong)NSArray<HomeBannerModel> *list;

@end

NS_ASSUME_NONNULL_END
