//
//  NetworkAdapter+Home.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/21.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter.h"
#import "HomeBannerModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface NetworkAdapter (Home)
// 获取首页banner
-(void)homeSendRequestToGetHomeBannerBlock:(void(^)(HomeBannerListModel *list))block;
@end

NS_ASSUME_NONNULL_END
