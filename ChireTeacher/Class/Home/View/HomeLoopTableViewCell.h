//
//  HomeLoopTableViewCell.h
//  BBFinance
//
//  Created by 裴烨烽 on 2018/6/19.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#import "PDBaseTableViewCell.h"

@interface HomeLoopTableViewCell : PDBaseTableViewCell

@property (nonatomic,strong)NSArray *transferMottoArr;

-(void)notifyActionWithBlock:(void(^)(NSInteger index))block;

@end
