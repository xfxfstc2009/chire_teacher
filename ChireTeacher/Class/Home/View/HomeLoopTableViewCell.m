//
//  HomeLoopTableViewCell.m
//  BBFinance
//
//  Created by 裴烨烽 on 2018/6/19.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#import "HomeLoopTableViewCell.h"
#import "XBTextLoopView.h"

static char notifyActionWithBlockKey;
@interface HomeLoopTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)XBTextLoopView *loopView;

@end

@implementation HomeLoopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // title
    self.titleLabel = [GWViewTool createLabelFont:@"13" textColor:@"红"];
    self.titleLabel.text = @"炽热箴言";
    CGSize titleSize = [Tool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(LCFloat(12), ([HomeLoopTableViewCell calculationCellHeight] - titleSize.height) / 2., titleSize.width, titleSize.height);
    [self addSubview:self.titleLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"EEEEEE"];
    self.lineView.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(11), ([HomeLoopTableViewCell calculationCellHeight] - LCFloat(35) / 2.) / 2., LCFloat(3) / 2., LCFloat(35) / 2.);
    [self addSubview:self.lineView];
    

}

-(void)setTransferMottoArr:(NSArray *)transferMottoArr{
    _transferMottoArr = transferMottoArr;
    if (!self.loopView && transferMottoArr.count){
        __weak typeof(self)weakSelf = self;
        
        self.loopView= [XBTextLoopView textLoopViewWith:transferMottoArr loopInterval:5 initWithFrame:CGRectMake(CGRectGetMaxX(self.lineView.frame) + LCFloat(11), 0, kScreenBounds.size.width - CGRectGetMaxX(self.lineView.frame) - LCFloat(11) - LCFloat(11), [HomeLoopTableViewCell calculationCellHeight] ) selectBlock:^(NSString *selectString, NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSInteger infoIndex) = objc_getAssociatedObject(strongSelf, &notifyActionWithBlockKey);
            if (block){
                block(index);
            }
        }];
        [self addSubview:self.loopView];
        self.loopView.userInteractionEnabled = NO;
    }
}

-(void)notifyActionWithBlock:(void(^)(NSInteger index))block{
    objc_setAssociatedObject(self, &notifyActionWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(75) / 2.;
}

@end
