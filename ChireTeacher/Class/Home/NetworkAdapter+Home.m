//
//  NetworkAdapter+Home.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/21.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "NetworkAdapter+Home.h"

@implementation NetworkAdapter (Home)

-(void)homeSendRequestToGetHomeBannerBlock:(void(^)(HomeBannerListModel *list))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:banner_img_list requestParams:nil responseObjectClass:[HomeBannerListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            HomeBannerListModel *bannerList = (HomeBannerListModel *)responseObject;
            if (block){
                block(bannerList);
            }
        }
    }];
}

@end
