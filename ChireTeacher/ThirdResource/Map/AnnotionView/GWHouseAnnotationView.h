//
//  GWHouseAnnotationView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>

@interface GWHouseAnnotationView : MAAnnotationView

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) UIImage *portrait;

@property (nonatomic, strong) UIView *calloutView;

-(void)locationShow:(void(^)())block;

@end
