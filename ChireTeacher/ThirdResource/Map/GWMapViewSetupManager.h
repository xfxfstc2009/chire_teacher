//
//  GWMapViewSetupManager.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

// 地图初始化

#import <Foundation/Foundation.h>

@interface GWMapViewSetupManager : NSObject

+(void)authorizineMap;

@end
