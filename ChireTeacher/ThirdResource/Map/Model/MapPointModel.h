//
//  MapPointModel.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/23.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "FetchModel.h"
//#import "OtherSearchSingleModel.h"
#import <MAMapKit/MAPointAnnotation.h>
@interface MapPointModel : FetchModel

@property (nonatomic,strong) MAPointAnnotation *pointAnnotation;
//@property (nonatomic,strong)OtherSearchSingleModel *liverSingleModel;

@end
