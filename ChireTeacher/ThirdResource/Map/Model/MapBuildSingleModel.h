//
//  MapBuildSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/30.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@protocol MapBuildSingleModel <NSObject>

@end

@interface MapBuildSingleModel : FetchModel

@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,assign)CGFloat lng;
@property (nonatomic,copy)NSString *location;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *time;
@property (nonatomic,assign)NSInteger distance;

@end



@interface MapBuildListModel : FetchModel

@property (nonatomic,strong)NSArray<MapBuildSingleModel> *list;

@end
