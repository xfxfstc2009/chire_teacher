//
//  MapBuidingAnnotationModel.h
//  17Live
//
//  Created by 裴烨烽 on 2018/1/24.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "MainCenterModel.h"

@interface MapBuidingAnnotationModel : MAPointAnnotation

@property (nonatomic,strong)MainCenterModel *transferCenterModel;
@property (nonatomic,strong)MACircle *circle;
@end
