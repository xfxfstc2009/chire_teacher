//
//  MapBuilderAnnotationView.m
//  17Live
//
//  Created by 裴烨烽 on 2018/1/24.
//  Copyright © 2018年 PPWhale. All rights reserved.
//

#import "MapBuilderAnnotationView.h"
#import "PDCustomCalloutView.h"
#import <objc/runtime.h>

#define kWidth  60.f
#define kHeight 60.f

#define kHoriMargin 5.f
#define kVertMargin 5.f

#define kPortraitWidth  50.f
#define kPortraitHeight 50.f

#define kCalloutWidth   250.0
#define kCalloutHeight  70.0

static char locationKey;
static char actionSelectedBlockKey;
@interface MapBuilderAnnotationView()
@property (nonatomic, strong) PDImageView *portraitImageView;
@property (nonatomic,strong)PDImageView *avatarImgView;                 /**< 头像*/
@property (nonatomic, strong) UILabel *nickNameLabel;;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)UIButton *actionButton;
@end

@implementation MapBuilderAnnotationView

#pragma mark - Handle Action

- (void)btnAction {
    CLLocationCoordinate2D coorinate = [self.annotation coordinate];

    NSLog(@"coordinate = {%f, %f}", coorinate.latitude, coorinate.longitude);
}

#pragma mark - Override

- (NSString *)name {
    return self.nickNameLabel.text;
}

- (void)setName:(NSString *)name {
    self.nickNameLabel.text = name;
}

- (UIImage *)portrait {
    return self.portraitImageView.image;
}

- (void)setPortrait:(UIImage *)portrait {
    self.portraitImageView.image = portrait;
}

- (void)setSelected:(BOOL)selected {
    [self setSelected:selected animated:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    if (self.selected == selected) {
        return;
    }
    void(^block)(BOOL isSelected) = objc_getAssociatedObject(self, &actionSelectedBlockKey);
    if (block){
        block (selected);
    }


    if (selected) {
        if (self.calloutView == nil) {
            self.calloutView = [[PDCustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
            self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x, -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
            self.calloutView.clipsToBounds = YES;

            // 2. 创建imgView
            self.avatarImgView = [[PDImageView alloc]init];
            self.avatarImgView.backgroundColor = [UIColor clearColor];
            self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(7), LCFloat(40),LCFloat(40));
            self.avatarImgView.layer.cornerRadius = LCFloat(3);
            self.avatarImgView.clipsToBounds = YES;
            [self.calloutView addSubview:self.avatarImgView];

            // 3. 创建nickName
            self.nickNameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
            self.nickNameLabel.font = [self.nickNameLabel.font boldFont];
            self.nickNameLabel.text = self.transferMainCenterModel.student.name;
            [self.calloutView addSubview:self.nickNameLabel];

            // 4. 创建其他信息
            self.dymicLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
            self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            self.dymicLabel.numberOfLines = 0;
            self.dymicLabel.text = self.transferMainCenterModel.student.name;
            self.dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            [self.calloutView addSubview:self.dymicLabel];

            self.actionButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            self.actionButton.frame = CGRectMake(self.calloutView.size_width - 50, 0,50, self.calloutView.size_height - kArrorHeight);
            [self.actionButton setTitle:@"操作" forState:UIControlStateNormal];
            [self.actionButton setBackgroundColor:[UIColor colorWithCustomerName:@"蓝"]];
            [self.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            __weak typeof(self)weakSelf = self;
            [self.actionButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                
            }];
            [self.calloutView addSubview:self.actionButton];

            // 2. 计算
            self.calloutView.size_height = LCFloat(11) + self.avatarImgView.size_height + LCFloat(7) + kArrorHeight;

            self.actionButton.frame = CGRectMake(self.calloutView.size_width - 50, 0,50, self.calloutView.size_height - kArrorHeight);
            self.actionButton.layer.mask = [self setButtoncornerRadius:UIRectCornerBottomRight | UIRectCornerTopRight Btn:self.actionButton cornerRadius:kradus];

            // 2. 昵称
            CGFloat width = self.actionButton.orgin_x - CGRectGetMaxX(self.avatarImgView.frame) - 2 * LCFloat(11);
            CGFloat margin = (self.actionButton.size_height - [NSString contentofHeightWithFont:self.nickNameLabel.font] - [NSString contentofHeightWithFont:self.dymicLabel.font]) / 3.;
            self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), margin, width, [NSString contentofHeightWithFont:self.nickNameLabel.font]);

            self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), CGRectGetMaxY(self.nickNameLabel.frame) + margin, width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
        }

        // 1.头像
        [self.avatarImgView uploadImageWithURL:self.transferMainCenterModel.student.avatar placeholder:nil callback:NULL];


        [self addSubview:self.calloutView];
    }
    else
    {
        [self.calloutView removeFromSuperview];
    }

    [super setSelected:selected animated:animated];
}


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL inside = [super pointInside:point withEvent:event];
    /* Points that lie outside the receiver’s bounds are never reported as hits,
     even if they actually lie within one of the receiver’s subviews.
     This can occur if the current view’s clipsToBounds property is set to NO and the affected subview extends beyond the view’s bounds.
     */
    if (!inside && self.selected)
    {
        inside = [self.calloutView pointInside:[self convertPoint:point toView:self.calloutView] withEvent:event];
    }

    return inside;
}

#pragma mark - Life Cycle

- (id)initWithAnnotation:(id<MAAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];

    if (self) {
        self.bounds = CGRectMake(0, 0, kWidth, kHeight);
        self.backgroundColor = [UIColor redColor];
        self.image = [UIImage imageNamed:@"login_thirdQQ_icon"];
        PDImageView *iconImageView = [[PDImageView alloc]init];
        iconImageView.image = [UIImage imageNamed:@"login_thirdQQ_icon"];
        iconImageView.frame = CGRectMake((kWidth - 18 ) / 2., (kHeight - 22) / 2., 18, 22);
        [self addSubview:iconImageView];

    }

    return self;
}



-(CAShapeLayer*)setButtoncornerRadius:(UIRectCorner)corners  Btn:(UIButton*)btn  cornerRadius:(float)cornerRadius {
    [btn.layer setMasksToBounds:YES];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:btn.bounds byRoundingCorners: corners cornerRadii: (CGSize){cornerRadius, cornerRadius}].CGPath;


    return maskLayer;
}

-(void)setTransferMainCenterModel:(MainCenterModel *)transferMainCenterModel{
    _transferMainCenterModel = transferMainCenterModel;
    PDImageView *mainImgView = [[PDImageView alloc]init];
    
    mainImgView.frame = CGRectMake(0, 0, LCFloat(50), LCFloat(50));
    __weak typeof(self)weakSelf = self;
    [mainImgView uploadImageRootWithURL:transferMainCenterModel.student.avatar placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSel = weakSelf;
        
    }];
    UIImage *img = [UIImage imageNamed:@"login_thirdQQ_icon"];
    self.image = img;
}


// 自定义宽高
- (UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize {
    UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
    [image drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return reSizeImage;
}

-(void)actionSelectedBlock:(void(^)(BOOL selected))block{
    objc_setAssociatedObject(self, &actionSelectedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
