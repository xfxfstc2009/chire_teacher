//
//  GWMapViewSetupManager.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/7/31.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWMapViewSetupManager.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>

@implementation GWMapViewSetupManager

+(void)authorizineMap{
    [AMapServices sharedServices].apiKey = (NSString *)mapIdentify;
    [AMapServices sharedServices].enableHTTPS = YES;

}


@end
