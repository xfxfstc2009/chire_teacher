//
//  MMHLocationNavigationManager.h
//  MamHao
//
//  Created by SmartMin on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【导航】
#import <Foundation/Foundation.h>
#import "PDCurrentLocationManager.h"
@interface MMHLocationNavigationManager : NSObject

#pragma mark - actionSheet
- (void)showActionSheetWithView:(UIView *)view shopName:(NSString *)shopName shopLocationLat:(CGFloat)shopLat shopLocationLon:(CGFloat)shopLon;

#pragma mark - actionSheet
- (void)showActionSheetWithTabBar:(UITabBar *)tabBar shopName:(NSString *)shopName shopLocationLat:(CGFloat)shopLat shopLocationLon:(CGFloat)shopLon;
@end
