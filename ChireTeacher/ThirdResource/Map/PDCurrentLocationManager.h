//
//  PDCurrentLocationManager.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 定位当前坐标

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>

@interface PDCurrentLocationManager : NSObject
// 全局的地址
@property (nonatomic,assign)CGFloat lng;
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,strong)AMapAddressComponent *addressComponent;

+ (PDCurrentLocationManager *)sharedLocation;

-(void)getCurrentLocationManager:(void(^)(CGFloat lat,CGFloat lng,AMapAddressComponent *addressComponent))block;


@end
