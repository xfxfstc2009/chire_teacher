//
//  StatusBarManager.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/30.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatusBarManager : NSObject
+(void)statusBarWithClear;
+(void)statusBarShowWithText:(NSString *)text;
+(void)statusBarHidenWithText:(NSString *)text;
+(void)statusBarWithText:(NSString *)text progress:(CGFloat)progress;
+(void)testStatusBarWithText:(NSString *)text progress:(CGFloat)progress;
@end
