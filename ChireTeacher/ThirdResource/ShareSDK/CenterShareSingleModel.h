//
//  CenterShareSingleModel.h
//  17Live
//
//  Created by 裴烨烽 on 2017/11/27.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "FetchModel.h"

@interface CenterShareSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *desc;
@property (nonatomic,copy)NSString *imgs;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *url;

@end
