//
//  GWTopSliderView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GWTopSliderValueViewDelegate <NSObject>
-(CGFloat)currentValueOffset;               // 当前的value
-(void)colorAnimationDidStart;              // 判断动画
-(void)popUpViewDidHide;                    // 判断是否隐藏

@end

@interface GWTopSliderView : UIView


@property (nonatomic,weak)id <GWTopSliderValueViewDelegate>delegate;
@property (nonatomic,assign)CGFloat cornerRadius;

-(UIColor *)color;               //颜色
- (void)setColor:(UIColor *)color;
-(UIColor *)opaqueColor;         // 颜色

- (void)setTextColor:(UIColor *)textColor;          // 文字颜色
- (void)setFont:(UIFont *)font;                     // 文字大小
- (void)setString:(NSString *)string;               // 文字内容
- (void)setAnimatedColors:(NSArray *)animatedColors withKeyTimes:(NSArray *)keyTimes;           // 设置动画内容
- (void)setAnimationOffset:(CGFloat)offset;
- (void)setArrowCenterOffset:(CGFloat)offset;
- (CGSize)popUpSizeForString:(NSString *)string;

- (void)show;
- (void)hide;



@end

