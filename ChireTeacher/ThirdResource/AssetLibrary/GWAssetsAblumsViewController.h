//
//  GWAssetsAblumsViewController.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/18.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//
// 所有相册内容
#import "AbstractViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

typedef void(^ablumSelectedBlock)(ALAssetsGroup *ablum);

@interface GWAssetsAblumsViewController : AbstractViewController

@property (nonatomic,strong)NSArray *ablumsArr;
@property (nonatomic,copy)ablumSelectedBlock ablumSelectrdBlock;

@end
