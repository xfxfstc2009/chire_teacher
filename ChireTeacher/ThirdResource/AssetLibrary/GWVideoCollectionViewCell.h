//
//  GWVideoCollectionViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface GWVideoCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong)AVCaptureSession *captureSession;                                    // 自定义摄像头

-(void)actionRunningCamera;
@end
