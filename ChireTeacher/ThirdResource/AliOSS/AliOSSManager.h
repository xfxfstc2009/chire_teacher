//
//  AliOSSManager.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/22.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSSSingleFileModel.h"
#import <AliyunOSSiOS/OSSService.h>
#import <AliyunOSSiOS/OSSCompat.h>

@interface AliOSSManager : NSObject

@property (nonatomic,strong)OSSClient *oSSClient;

+(instancetype)sharedOssManager;

#pragma mark - 上传文件信息
-(void)uploadFileWithType:(OSSUploadType)type name:(NSString *)objcName obj:(OSSSingleFileModel *)obj block:(void(^)(NSString *fileUrl))block progress:(void(^)(CGFloat progress))progressBlock;

@end
