//
//  OSSSingleFileModel.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/30.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import "FetchModel.h"

@protocol OSSSingleFileModel <NSObject>

@end

@interface OSSSingleFileModel : FetchModel

@property (nonatomic,copy)NSString *objcName;           /**< 文件名字*/
@property (nonatomic,strong)UIImage *objcImg;           /**< 文件*/
@property (nonatomic,strong)NSData *objcData;           /**< 文件*/
@property (nonatomic,copy)NSString *fileType;           /**< 文件类型*/
@property (nonatomic,copy)NSString *fileUrl;

@end
