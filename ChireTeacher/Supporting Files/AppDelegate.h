//
//  AppDelegate.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/5.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

