//
//  main.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/5.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
