
//
//  Constants.h
//  BBFinance
//
//  Created by 裴烨烽 on 2018/6/12.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#ifndef Constants_h
#define Constants_h


// 【USER Detault】
#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER     [NSNotificationCenter defaultCenter]

// 【System】
#define IOS_Version [UIDevice currentDevice].systemVersion.floatValue
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)
#define IS_IOS9_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 8.99)
#define IS_IOS10_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 9.99)
#define IS_IOS11_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 10.99)
#define IS_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5  ([UIScreen mainScreen].bounds.size.height == 568)
#define iPhone6  ([UIScreen mainScreen].bounds.size.height == 667)
#define iPhone6Plus  ([UIScreen mainScreen].bounds.size.height == 736)
#define ipadMini2  ([UIScreen mainScreen].bounds.size.height == 1024)

// 【屏幕尺寸】
#define kScreenBounds               [[UIScreen mainScreen] bounds]

// 【Style】
#define RGB(r, g, b,a)    [UIColor colorWithRed:(r)/255. green:(g)/255. blue:(b)/255. alpha:a]
#define BACKGROUND_VIEW_COLOR        RGB(239, 239, 244,1)               // 主题色
#define NAVBAR_COLOR RGB(255, 255, 255,1)                               // navBar

// 【nav】
#define BAR_BUTTON_FONT       [UIFont systemFontOfSize:14.]
#define BAR_MAIN_TITLE_FONT   [UIFont boldSystemFontOfSize:18.]
#define BAR_SUB_TITLE_FONT    [UIFont systemFontOfSize:12.]
#define BAR_TITLE_PADDING_TOP -3.
#define BAR_TITLE_MAX_WIDTH   LCFloat(230)

// 【log】
// 【Log 方式】
#ifdef DEBUG
#   define PDLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define PDLog(...)
#endif

// 【高德地图】
#define mapIdentify @"c025925f09a92640f032cd212c8653af"

// 【百川聊天】
#define AliChattingIdentify @"23330943"                                     // 百川chattingAPI
#define AliPush @"3448665"
#define AliIMKey @"24808439"
#define AliIMSecret @"6702362ca6456c28eaf27fdb95337c6f"

// 【阿里推送】
#define AliPushKey @"24789645"
#define AliPushAppSecret @"d8acac3995b0c2e3daabca2e3a83ef31"

// 【是否layer】
#define View_Layer @"View_Layer"                    // 是否边框

// 【debug状态下的地址】
#define testNet_address @"testNet_address"          // 端口号
#define testNet_port @"testNet_port"                //port

// 【用户自定义配置】
#define mainStyleColor @"mainStyleColor"
#define UserDefaultCustomerType    @"CustomerType"                             // 游客类型
#define UserDefaultUserToken @"UserDefaultUserToken"
#define UserDefaultCustomerMemberId @"CustomerMemberId"
#define UserDetailtCustomeSocketAddress @"UserDetailtCustomeSocketAddress"      // socket的地址
#define UserDetailtCustomeSocketPort @"UserDetailtCustomeSocketPort"            // socket的端口

// 【三方分享】
#define WeChatAppID         @"wx9604df71fe171c03"
#define WeChatAppSecret     @"5298a6704e01f6dba2b794ba2e5ba0ac"

#define QQAppID             @"1105501406"
#define QQAppAppSecret      @"a0zBvC0I5boEIliE"

#define WeiBoAPPID          @"1938399542"
#define WeiBoAppSecret      @"b888b2779298f00b98aa6276f8cb3bdc"
#define WeiBoRedirectUri    @"http://www.pandaol.com"

// 【阿里OSS】
#define ossAccessKey    @"LTAIcQxwreHhXLrt"
#define ossSecretKey @"CMY8mTxvbgFR4SLyJwxQsFHUZtfD65"
#define ossEndpoint   @"http://oss-cn-hangzhou.aliyuncs.com"
#define ossBucketName @"bee-tv"
#define aliyunOSS_BaseURL @"https://ppwhale.oss-cn-shanghai.aliyuncs.com/"

// 【分页每页数量】
static NSInteger cutPageNum = 10;

// 腾讯云
#define QQLiveID @"1400049832"
#define QQAccountType @"19334"


static NSString *testURL = @"http://d.ifengimg.com/mw600/img1.ugc.ifeng.com/newugc/20170410/9/wemedia_h5/e12537bd4e740e208ccfc048754325229ac3e465_size1235_w3000_h2002.jpeg";


#define LoginType @"loginType"              // 登录类型
#define LoginKey @"loginKey"                // 登录key

#endif /* Constants_h */
