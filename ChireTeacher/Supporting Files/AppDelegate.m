//
//  AppDelegate.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/5.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "AppDelegate.h"
#import "PDMainTabbarViewController.h"
#import "GWMapViewSetupManager.h"
#import "PDLaungchViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self mainSetup];
    [GWMapViewSetupManager authorizineMap];                                                  // 1.添加地图
    [self createRootView];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {

}


- (void)applicationDidEnterBackground:(UIApplication *)application {

}


- (void)applicationWillEnterForeground:(UIApplication *)application {

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
   
}


- (void)applicationWillTerminate:(UIApplication *)application {

}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if (self.window) {
        if (url) {
            NSString *fileName = url.lastPathComponent; // 从路径中获得完整的文件名（带后缀）
            // path 类似这种格式：
            NSString *path = url.absoluteString; // 完整的url字符串
            path = [self URLDecodedString:path]; // 解决url编码问题
            
            NSMutableString *string = [[NSMutableString alloc] initWithString:path];
            
            if ([path hasPrefix:@"file://"]) { // 通过前缀来判断是文件
                // 去除前缀：/private/var/mobile/Containers/Data/Application/83643509-E90E-40A6-92EA-47A44B40CBBF/Documents/Inbox/jfkdfj123a.pdf
                [string replaceOccurrencesOfString:@"file://" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, path.length)];
                
                // 此时获取到文件存储在本地的路径，就可以在自己需要使用的页面使用了
                NSDictionary *dict = @{@"fileName":fileName,
                                       @"filePath":string};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"FileNotification" object:nil userInfo:dict];
                
//                 NSURL *URL = [NSURL URLWithString:kBlogImageStr];
                NSData *dataCustom = [[NSData alloc] initWithContentsOfURL:url];
                NSArray *fileArr = [fileName componentsSeparatedByString:@"."];
                NSString *fileRootName = @"";
                NSString *fileRootType = @"";
                if (fileArr.count == 2){
                    fileRootName = [fileArr objectAtIndex:0];
                    fileRootType = [fileArr objectAtIndex:1];
                }
                OSSSingleFileModel *ossFileModel = [[OSSSingleFileModel alloc]init];
                ossFileModel.objcName = fileRootName;
                ossFileModel.objcData = dataCustom;
                ossFileModel.fileType = fileRootType;
                
                [AccountModel sharedAccountModel].ossFileModel = ossFileModel;
                return YES;
            }
        }
    }
    return YES;
}

// 当文件名为中文时，解决url编码问题
- (NSString *)URLDecodedString:(NSString *)str {
    NSString *decodedString=(__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(NULL, (__bridge CFStringRef)str, CFSTR(""), CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    
    return decodedString;
}


#pragma mark - createMainView
-(void)mainSetup{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor clearColor];
    [self.window makeKeyAndVisible];
}

-(void)createRootView{
    PDMainTabbarViewController *tabBarController = [PDMainTabbarViewController sharedController];
    self.window.rootViewController = tabBarController;
    
//    PDLaungchViewController *laungchVC = [[PDLaungchViewController alloc]init];
//    [self.window addSubview:laungchVC.view];
}

@end
