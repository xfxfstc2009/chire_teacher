//
//  EnumType.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/1/18.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "FetchModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger,SignType) {
    SignTypeNoSignIn,               /**< 未签到*/
    SignTypeSignIn,                 /**< 已签到*/
    SignTypeNoSignOut,              /**< 没签退*/
    SignTypeSignOut,                /**< 签退*/
};

@interface EnumType : FetchModel


@end

NS_ASSUME_NONNULL_END
