//
//  DropListSingleCell.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropListSingleCell : UITableViewCell{
    BOOL			isChecked;
}


@property (nonatomic,copy)NSString *transferFixedStr;                   /**< 前面的文字*/
@property (nonatomic,assign)CGFloat transferCellHeight;

-(void)setChecked:(BOOL)checked;
+(CGFloat)calculationCellHeight;
@end
