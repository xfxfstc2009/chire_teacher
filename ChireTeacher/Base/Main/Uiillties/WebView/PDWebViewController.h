//
//  PDWebViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDWebView.h"

@interface PDWebViewController : AbstractViewController
@property (nonatomic,strong)PDWebView *webView;

-(void)webDirectedWebUrl:(NSString *)url;
-(void)webViewControllerWithAddress:(NSString *)url;

@end
