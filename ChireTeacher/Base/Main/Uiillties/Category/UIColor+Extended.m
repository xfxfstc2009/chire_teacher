//
//  UIColor+Extended.m
//  LaiCai
//
//  Created by SmartMin on 15-8-10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UIColor+Extended.h"

@implementation UIColor (Extended)

+ (UIColor *)hexChangeFloat:(NSString *)hexColor{
    if (hexColor.length < 6){
        return nil;
    }
    unsigned int red_,green_,blue_;
    NSRange exceptionRange;
    exceptionRange.length = 2;
    
    // red
    exceptionRange.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:exceptionRange]]scanHexInt:&red_];
    
    //green
    exceptionRange.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:exceptionRange]]scanHexInt:&green_];
    
    //blue
    exceptionRange.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:exceptionRange]]scanHexInt:&blue_];
    
    UIColor *resultColor = [UIColor colorWithRed:(CGFloat)red_/255. green:(CGFloat)green_/255. blue:(CGFloat)blue_/255. alpha:1.0];
    return resultColor;
}

#pragma mark 自定义颜色
+(UIColor *)colorWithCustomerName:(NSString *)colorName{
    UIColor *customerColor;
    if ([colorName isEqualToString:@"黑"]){
        customerColor = c2;
    } else if ([colorName isEqualToString:@"灰"]){
        customerColor = [UIColor hexChangeFloat:@"666666"];
    } else if ([colorName isEqualToString:@"浅灰"]){
        customerColor = [UIColor hexChangeFloat:@"989898"];
    } else if ([colorName isEqualToString:@"深灰"]){
        customerColor = [UIColor hexChangeFloat:@"333333"];
    } else if ([colorName isEqualToString:@"红"]){
        customerColor = UURed;
    } else if ([colorName isEqualToString:@"白"]){
        customerColor = [UIColor hexChangeFloat:@"ffffff"];
    } else if ([colorName isEqualToString:@"粉"]){
        customerColor =  [UIColor colorWithRed:244/255. green:149/255. blue:180/255. alpha:1];
    } else if ([colorName isEqualToString:@"淡灰"]){
        customerColor = [UIColor hexChangeFloat:@"858585"];
    } else if ([colorName isEqualToString:@"分割线"]){
        customerColor =  [UIColor colorWithRed:242/255. green:242/255. blue:242/255. alpha:1];
    } else if ([colorName isEqualToString:@"背景"]){
        customerColor = [UIColor hexChangeFloat:@"f6f6f6"];
    } else if ([colorName isEqualToString:@"红高亮"]){
        customerColor = [UIColor hexChangeFloat:@"c95362"];
    } else if ([colorName isEqualToString:@"白高亮"]){
        customerColor = [UIColor hexChangeFloat:@"f2f2f2"];
    } else if ([colorName isEqualToString:@"失效"]){
        customerColor = [UIColor hexChangeFloat:@"bfbebe"];
    } else if ([colorName isEqualToString:@"快递"]){
        customerColor = [UIColor hexChangeFloat:@"5ccccc"];
    } else if ([colorName isEqualToString:@"蓝"]){
        customerColor = [UIColor hexChangeFloat:@"447ed8"];
    } else if ([colorName isEqualToString:@"绿"]){
        customerColor = [UIColor colorWithRed:16/255. green:198/255. blue:65/255. alpha:1];
    } else if ([colorName isEqualToString:@"灰色3"]){
        customerColor = [UIColor hexChangeFloat:@"eeeeee"];
    } else if ([colorName isEqualToString:@"紫"]){
        customerColor = [UIColor colorWithRed:115/256. green:135/256. blue:198/256. alpha:1];
    } else if ([colorName isEqualToString:@"浅蓝"]){
        customerColor = [UIColor colorWithRed:135/255. green:214/255. blue:249/255. alpha:1];
    } else if ([colorName isEqualToString:@"黄"]){
        customerColor = [UIColor colorWithRed:255/255. green:214/255. blue:79/255. alpha:1];
    } else if ([colorName isEqualToString:@"深绿"]){
        customerColor = [UIColor colorWithRed:51/255. green:177/255. blue:92/255. alpha:1];
    } else if ([colorName isEqualToString:@"浅黑"]){      //协议字体颜色
        customerColor = [UIColor colorWithRed:40/255. green:40/255. blue:40/255. alpha:1];
    } else if ([colorName isEqualToString:@"白灰"]){
        customerColor = [UIColor colorWithRed:242/255. green:242/255. blue:242/255. alpha:1];
    } else if ([colorName isEqualToString:@"橙"]){
        customerColor = c24;
    } else if ([colorName isEqualToString:@"金"]){
        customerColor = c26;
    } else if ([colorName isEqualToString:@"lol"]){
        customerColor = RGB(0, 140, 217, 1);
    } else if ([colorName isEqualToString:@"cs"]){
        customerColor = RGB(0, 186, 123, 1);
    } else if ([colorName isEqualToString:@"dota"]){
        customerColor = RGB(213, 0, 43, 1);
    } else if ([colorName isEqualToString:@"ow"]){
        customerColor = RGB(255, 154, 0, 1);
    } else if ([colorName isEqualToString:@"pvp"]){
        customerColor = RGB(85, 93, 219, 1);
    } else if ([colorName isEqualToString:@"侧边分割线"]){
        customerColor = [UIColor hexChangeFloat:@"212223"];
    } else if ([colorName isEqualToString:@"土"]){
        customerColor = RGB(204, 172, 111, 1);
    } else if ([colorName isEqualToString:@"地图蓝"]){
        customerColor = [UIColor colorWithRed:135/255. green:214/255. blue:249/255. alpha:.4f];
    } else if ([colorName isEqualToString:@"地图灰"]){
        customerColor = [UIColor colorWithRed:242/255. green:242/255. blue:242/255. alpha:.4f];
    } else if ([colorName isEqualToString:@"棕黑"]){
        customerColor = [UIColor hexChangeFloat:@"c95362"];
    } else {
        customerColor = [UIColor hexChangeFloat:colorName];
    }

    
    return customerColor;
}

- (NSInteger)getHexColor {
    CGFloat rComponent;
    CGFloat bComponent;
    CGFloat gComponent;
    
    [self getRed:&rComponent green:&gComponent blue:&bComponent alpha:nil];
    
    
    return lroundf(rComponent * 255) * 256 * 256 + lroundf(gComponent * 255) * 256  + lroundf(bComponent * 255);
}




+ (UIColor *)colorWithHex:(NSInteger)intColor {
    CGFloat bComponent = intColor & 0xFF;
    CGFloat gComponent = intColor >> 8 & 0xFF;
    CGFloat rComponent = intColor >> 16 & 0xFF;
    
    return [UIColor colorWithRed:rComponent/255.0 green:gComponent/255.0 blue:bComponent/255.0 alpha:1];
}
@end
