//
//  UILabel+PDCategory.m
//  PandaKing
//
//  Created by Cranz on 16/9/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UILabel+PDCategory.h"
#import <objc/runtime.h>
#import <POP/POP.h>

static const char * constraintSizeKey = "constraintSizeKey";
@implementation UILabel (PDCategory)
@dynamic constraintSize;

- (CGSize)size {
    return [self.text sizeWithCalcFont:self.font constrainedToSize:self.constraintSize];
}

- (void)setConstraintSize:(CGSize)constraintSize {
    objc_setAssociatedObject([self class], constraintSizeKey, [NSValue valueWithCGSize:constraintSize], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGSize)constraintSize {
    NSValue *sizeValue = objc_getAssociatedObject([self class], constraintSizeKey);
    if (sizeValue) {
        return [sizeValue CGSizeValue];
    } else {
        return CGSizeZero;
    }
}

-(void)animationWithScrollToValue:(NSInteger)toValue{
    [self pop_removeAllAnimations];
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
        };
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);
    anim.toValue = @(toValue);
    
    [self pop_addAnimation:anim forKey:@"counting"];

}




@end
