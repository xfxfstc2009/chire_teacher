//
//  UITabBarController+Autorotate.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "UITabBarController+Autorotate.h"

@implementation UITabBarController (Autorotate)

- (BOOL)shouldAutorotate

{
    
    return [self.selectedViewController shouldAutorotate];
    
}


- (NSUInteger)supportedInterfaceOrientations

{
    
    return [self.selectedViewController supportedInterfaceOrientations];
    
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return [self.selectedViewController preferredInterfaceOrientationForPresentation];
    
}

@end
