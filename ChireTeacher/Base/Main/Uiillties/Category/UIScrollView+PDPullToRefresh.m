//
//  UIScrollView+PDPullToRefresh.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIScrollView+PDPullToRefresh.h"
#import "UIScrollView+PullToRefreshCoreText.h"
#import <SVPullToRefresh.h>
#import <objc/runtime.h>
static BOOL isSuper = YES;

static char pageKey;
static char isXiaLaKey;              //
static char setHasLoadKey;
@implementation UIScrollView (PDPullToRefresh)



-(void)appendingPullToRefreshHandler:(void (^)())block{             // 下啦刷新
    // 修改page = 0
    self.currentPage = 1;
    self.hasLoad = YES;
    __weak typeof(self)weakSelf = self;
    if(isSuper){
        [self addPullToRefreshWithPullText:@"炽热教育" action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.currentPage = 1;
            strongSelf.isXiaLa = YES;
            if (block){
                block();
            }
            strongSelf.hasLoad = NO;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                strongSelf.hasLoad = YES;
            });
        }];
    } else{
        [self addPullToRefreshWithActionHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.currentPage = 1;
            strongSelf.isXiaLa = YES;
            if (block){
                block();
            }
        }];
        
        SVPullToRefreshView *refreshView = (SVPullToRefreshView *)self.pullToRefreshView;
        CGRect rect = refreshView.frame;
        rect.origin.x = 30.;
        refreshView.frame = rect;
        refreshView.arrowColor = RGB(197, 197, 197, 1);
        refreshView.textColor = RGB(197, 197, 197,1);
        [refreshView setTitle:@"下拉刷新数据" forState:SVPullToRefreshStateStopped];
        [refreshView setTitle:@"释放进行刷新" forState:SVPullToRefreshStateTriggered];
        [refreshView setTitle:@"正在加载..." forState:SVPullToRefreshStateLoading];
    }
}

-(void)stopPullToRefresh{
    [self finishLoading];
}

-(void)appendingFiniteScrollingPullToRefreshHandler:(void (^)())block{              // 上啦加载


    __weak typeof(self)weakSelf = self;
    [self addInfiniteScrollingWithActionHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!strongSelf.hasLoad){
            return;
        }
        strongSelf.currentPage ++;
        strongSelf.isXiaLa = NO;
        if (block){
            block();
        }
    }];
}

-(void)stopFinishScrollingRefresh{
    [self.infiniteScrollingView stopAnimating];
}

-(void)showBottomTitle{
    UILabel *endLabel = (UILabel *)[self viewWithTag:1000];
    endLabel.hidden = NO;
}

-(void)hiddenBottomTitle{
    UILabel *endLabel = (UILabel *)[self viewWithTag:1000];
    endLabel.hidden = YES;
}


#pragma mark - Properties

-(void)setCurrentPage:(NSInteger)currentPage{
    objc_setAssociatedObject(self, &pageKey, @(currentPage), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSInteger)currentPage{
    NSInteger currrentPage = [objc_getAssociatedObject(self, &pageKey) integerValue];
    return currrentPage;
}

-(void)setIsXiaLa:(BOOL)isXiaLa{
    objc_setAssociatedObject(self, &isXiaLaKey, @(isXiaLa), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isXiaLa{
    BOOL xiala = [objc_getAssociatedObject(self, &isXiaLaKey) integerValue];
    return xiala;
}

-(void)setHasLoad:(BOOL)hasLoad{
    objc_setAssociatedObject(self, &setHasLoadKey, @(hasLoad), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)hasLoad{
    BOOL hasLoad = [objc_getAssociatedObject(self, &setHasLoadKey) integerValue];
    return hasLoad;
}

@end
