//
//  PDMainTabbarViewController.h
//  BBFinance
//
//  Created by 裴烨烽 on 2018/6/12.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#import "ESTabBarController.h"

@interface PDMainTabbarViewController : ESTabBarController

@property (nonatomic,assign)CGFloat tabBarHeight;
@property (nonatomic,assign)CGFloat navBarHeight;

+(instancetype)sharedController;

@end
