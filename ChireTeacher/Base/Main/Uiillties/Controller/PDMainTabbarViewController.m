//
//  PDMainTabbarViewController.m
//  BBFinance
//
//  Created by 裴烨烽 on 2018/6/12.
//  Copyright © 2018年 川普投资. All rights reserved.
//

#import "PDMainTabbarViewController.h"
#import "HomeRootViewController.h"
#import "MainCenterMenuViewController.h"

@interface PDMainTabbarViewController ()

@end

@implementation PDMainTabbarViewController


+(instancetype)sharedController{
    static PDMainTabbarViewController *_sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray *iconArr = @[@"icon_tabbar_home_nor",@"icon_tabbar_home_nor"];
        _sharedController = [[PDMainTabbarViewController alloc] initWithTabIconNames:iconArr];
        _sharedController.selectedColor = [UIColor colorWithCustomerName:@"黑"];
        _sharedController.buttonsBackgroundColor = BACKGROUND_VIEW_COLOR;
        _sharedController.separatorLineVisible = YES;
        _sharedController.separatorLineColor = [UIColor lightGrayColor];
        _sharedController.selectionIndicatorHeight = 1;
        
        // 1. 首页
        HomeRootViewController *homeViewController = [[HomeRootViewController alloc]init];
        UINavigationController *homeRootNav = [[UINavigationController alloc] initWithRootViewController:homeViewController];;
        homeRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        homeRootNav.navigationBar.layer.shadowOpacity = .2f;
        homeRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:homeRootNav atIndex:0];

        // 2. 班级
        MainCenterMenuViewController *classViewController = [MainCenterMenuViewController sharedAccountModel];
        UINavigationController *classRootNav = [[UINavigationController alloc] initWithRootViewController:classViewController];;
        classRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        classRootNav.navigationBar.layer.shadowOpacity = .2f;
        classRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:classRootNav atIndex:1];
        
    });
    return _sharedController;
}

-(CGFloat)tabBarHeight{
    if ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO){
        return  49 + 34;
    } else {
        return  49 ;
    }
}
-(CGFloat)navBarHeight{
    if ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO){
        return  44 + 44;
    } else {
        return  20 + 44;
    }
}





@end
