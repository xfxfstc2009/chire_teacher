//
//  AbstractViewController.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/26.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AbstractViewController : UIViewController

#pragma mark - 属性
@property (nonatomic,copy,nullable)NSString *barMainTitle;                       /**< 标题*/
@property (nonatomic,copy,nullable)NSString *barSubTitle;                        /**< 副标题*/
@property (nonatomic,assign)BOOL hasCancelPanDismiss;                            /**< 该页面是否左滑动删除*/

- (nonnull UIButton *)leftBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock ;

- (nonnull UIButton *)rightBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock ;

// LoginManager
- (void)authorizeWithCompletionHandler:(nullable void(^)(BOOL successed))handler;

// cell Manager
-(NSInteger)cellIndexPathSectionWithcellData:(nullable NSString *)string sourceArr:(nullable NSArray *)array;
-(NSInteger )cellIndexPathRowWithcellData:(nullable NSString *)string sourceArr:(nullable NSArray *)array;
@end
