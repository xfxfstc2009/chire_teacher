//
//  PDImageView.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "PDImageView.h"

@implementation PDImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
     [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeNormal callback:callbackBlock];
}

-(void)uploadImageRootWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeRoot callback:callbackBlock];
}

// 更新用户头像图片
-(void)uploadImageWithAvatarURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    [self uploadMainImageWithURL:urlString placeholder:placeholder imgType:PDImgTypeAvatar callback:callbackBlock];
}


#pragma mark - 更新图片 主方法
-(void)uploadMainImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder imgType:(PDImgType)type callback:(void(^)(UIImage *image))callbackBlock{
    if ((![urlString isKindOfClass:[NSString class]]) ||(!urlString.length)){
        return;
    }
    // 1. 判断placeholder
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"placeholder.jpg"];
    }
    
    // 2. 判断url
    NSString *mainImgUrl = @"";

    mainImgUrl = [NSString stringWithFormat:@"%@%@?x-oss-process=image/resize,m_fill,w_%li,h_%li,limit_0/auto-orient,1/format,png",aliyunOSS_BaseURL,urlString,(long)self.size_width,(long)self.size_height];

    NSURL *imgUrl = [NSURL URLWithString:mainImgUrl];
    
    // 3. 进行加载图片
    [self sd_setImageWithURL:imgUrl placeholderImage:placeholder completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        if (type == PDImgTypeNormal){
            self.image = image;
        } else if (type == PDImgTypeAvatar){
            self.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round.png"]];
        }
        
        if (callbackBlock){
            callbackBlock(self.image);
        }
    }];
}

#pragma mark - 清除缓存
+(void)cleanImgCacheWithBlock:(void(^)())block{
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    __weak typeof(self)weakSelf = self;
    [[SDWebImageManager sharedManager].imageCache clearDiskOnCompletion:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

+(NSString *)diskCount{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    NSString *clearCacheName = tempSizeWithM >= 1 ? [NSString stringWithFormat:@"%.2fM",tempSizeWithM] : [NSString stringWithFormat:@"%.2fK",tempSizeWithM * 1024];
    return clearCacheName;
}

+(CGFloat)diskCountFloat{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    return tempSizeWithM;
}

+(NSString *)appendingImgUrl:(NSString *)urlString{
    NSString *mainImgUrl = @"";
    if ([urlString hasPrefix:@"http://"]){
        mainImgUrl = urlString;
    } else {
        mainImgUrl = [NSString stringWithFormat:@"%@%@?x-oss-process=image/resize,m_fill,w_100,h_100,limit_0/auto-orient,1/format,png",aliyunOSS_BaseURL,urlString];
    }
    return mainImgUrl;
}

@end
