//
//  GWInputTextFieldTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWInputTextFieldTableViewCell.h"

static char textFieldKey;
@interface GWInputTextFieldTableViewCell()
@property (nonatomic,strong)UILabel *fixedLabel;                    /**< 标题*/

@end

@implementation GWInputTextFieldTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [self addSubview:self.fixedLabel];
    
    NSString *str = @"账号密码";
    CGSize fixedSize = [str sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, fixedSize.width, LCFloat(44));
    
    //2.创建输入框
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.inputTextField];
    self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(7), 0, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.fixedLabel.frame) - LCFloat(7), self.transferCellHeight);
    [self.inputTextField addTarget:self action:@selector(textFieldDidChanged) forControlEvents:UIControlEventEditingChanged];

}

-(void)setTransferPlaceholeder:(NSString *)transferPlaceholeder {
    _transferPlaceholeder = transferPlaceholeder;
    self.inputTextField.placeholder = transferPlaceholeder;
    [self auCalculationFrame];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.fixedLabel.text = transferTitle;
    [self auCalculationFrame];
}

-(void)setTransferInputText:(NSString *)transferInputText{
    _transferInputText = transferInputText;
    self.inputTextField.text = transferInputText;
}

-(void)auCalculationFrame{
    if (self.transferTitle.length){
        self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, self.fixedLabel.size_width, self.transferCellHeight);
        self.fixedLabel.hidden = NO;
        
        self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(7), 0, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.fixedLabel.frame) - LCFloat(7), self.transferCellHeight);
    } else {
        self.fixedLabel.hidden = YES;
        self.inputTextField.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), self.transferCellHeight);
        
    }
}

-(void)textFieldDidChanged{
    void(^block)(NSString *info) = objc_getAssociatedObject(self, &textFieldKey);
    if (block){
        block(self.inputTextField.text);
    }
}

-(void)textFieldDidChangeBlock:(void (^)(NSString *))block{
    objc_setAssociatedObject(self, &textFieldKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 计算高度
+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}

@end
