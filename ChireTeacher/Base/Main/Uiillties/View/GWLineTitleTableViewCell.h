//
//  GWLineTitleTableViewCell.h
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/1.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "PDBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface GWLineTitleTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;

+(CGFloat)calculationCellHeight;

@end

NS_ASSUME_NONNULL_END
