//
//  GWIconTableViewCell.m
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/6.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "GWIconTableViewCell.h"

@interface GWIconTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;               /**< iconImg*/
@property (nonatomic,strong)PDImageView *arrowImageView;

@end

@implementation GWIconTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    // 2. 创建标题
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.titleLabel];
    
    // 3. 创建dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    self.dymicLabel.textAlignment = NSTextAlignmentLeft;
    self.dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.dymicLabel];
    
    // 4. 创建箭头
    self.arrowImageView = [[PDImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImageView];
}


// icon
-(void)setTransferIcon:(UIImage *)transferIcon{
    _transferIcon = transferIcon;
    self.iconImgView.image = transferIcon;
    [self.iconImgView sizeToFit];
    
    self.iconImgView.orgin_x = LCFloat(11);
    self.iconImgView.center_y = self.transferCellHeight / 2.;
    
    [self aucalculationFrame];
}

//title
-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(LCFloat(11), 0, titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    [self aucalculationFrame];
}

//desc
-(void)setTransferDesc:(NSString *)transferDesc{
    _transferDesc = transferDesc;
    self.dymicLabel.text = transferDesc;
    [self aucalculationFrame];
}

//arrow
-(void)setTransferHasArrow:(BOOL)transferHasArrow{
    _transferHasArrow = transferHasArrow;
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(9), (self.transferCellHeight - LCFloat(15)) / 2., LCFloat(9), LCFloat(15));
    [self aucalculationFrame];
}

-(void)aucalculationFrame{
    
    // 1. 计算标题
    if(self.transferIcon){
        self.titleLabel.orgin_x = CGRectGetMaxX(self.iconImgView.frame) + LCFloat(11);
    } else {
        self.titleLabel.orgin_x = LCFloat(11);
    }
    
    // 2. 计算箭头
    CGFloat width = 0 ;
    if (self.transferHasArrow){         // 如果有箭头
        self.arrowImageView.hidden = NO;
        width = self.arrowImageView.orgin_x - CGRectGetMaxX(self.titleLabel.frame) - LCFloat(11) - LCFloat(5);
    } else{
        width = kScreenBounds.size.width - CGRectGetMaxX(self.titleLabel.frame) - 2 * LCFloat(11);
        self.arrowImageView.hidden = YES;
    }

    // 3. title
    CGFloat margin = (self.transferCellHeight - [NSString contentofHeightWithFont:self.titleLabel.font] - [NSString contentofHeightWithFont:self.dymicLabel.font]) / 3.;
    self.titleLabel.orgin_y = margin;
    
    self.dymicLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + margin, self.titleLabel.size_width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}


@end
