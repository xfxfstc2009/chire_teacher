//
//  GWNormalTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWNormalTableViewCell.h"

@interface GWNormalTableViewCell()
@property (nonatomic,strong)PDImageView *iconImageView;
@property (nonatomic,strong)PDImageView *arrowImageView;

@end

@implementation GWNormalTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    self.iconImageView = [[PDImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImageView];
    
    // 2. 创建标题
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.titleLabel];

    // 3. 创建dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    self.dymicLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.dymicLabel];
    
    // 4. 创建箭头
    self.arrowImageView = [[PDImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImageView];
}

// icon
-(void)setTransferIcon:(UIImage *)transferIcon{
    _transferIcon = transferIcon;
    self.iconImageView.image = transferIcon;
    [self.iconImageView sizeToFit];

    self.iconImageView.orgin_x = LCFloat(11);
    self.iconImageView.center_y = self.transferCellHeight / 2.;

    [self aucalculationFrame];
}

//title
-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(LCFloat(11), 0, titleSize.width, self.transferCellHeight);
    
    [self aucalculationFrame];
}

//desc
-(void)setTransferDesc:(NSString *)transferDesc{
    _transferDesc = transferDesc;
    self.dymicLabel.text = transferDesc;
    [self aucalculationFrame];
}

//arrow
-(void)setTransferHasArrow:(BOOL)transferHasArrow{
    _transferHasArrow = transferHasArrow;
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(9), (self.transferCellHeight - LCFloat(15)) / 2., LCFloat(9), LCFloat(15));
    [self aucalculationFrame];
}

-(void)aucalculationFrame{
    
    // 1. 计算标题
    
    if(self.transferIcon){
        self.titleLabel.orgin_x = CGRectGetMaxX(self.iconImageView.frame) + LCFloat(5);
    } else {
        self.titleLabel.orgin_x = LCFloat(11);
    }
    
    // 2. 计算箭头
    CGFloat width = 0 ;
    if (self.transferHasArrow){         // 如果有箭头
        self.arrowImageView.hidden = NO;
        width = self.arrowImageView.orgin_x - CGRectGetMaxX(self.titleLabel.frame) - LCFloat(11) - LCFloat(5);
    } else{
        width = kScreenBounds.size.width - CGRectGetMaxX(self.titleLabel.frame) - 2 * LCFloat(11);
        self.arrowImageView.hidden = YES;
    }
    self.dymicLabel.frame =CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(11), 0, width, self.transferCellHeight);
    
}

-(void)setTextAlignmentCenter:(BOOL)textAlignmentCenter{
    _textAlignmentCenter = textAlignmentCenter;
    if (textAlignmentCenter){
        self.titleLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - LCFloat(11) * 2, self.transferCellHeight);
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}

@end
