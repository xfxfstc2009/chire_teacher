//
//  GWNumberChooseView.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWNumberChooseView : UIView

-(instancetype)initWithFrame:(CGRect)frame numberBlock:(void(^)(NSInteger number))block;

@property (nonatomic,assign)NSInteger currentNumber;            /**< 当前的数字*/

@end
