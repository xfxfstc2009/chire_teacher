//
//  PDImageView.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+WebCache.h>

typedef NS_ENUM(NSInteger ,PDImgType) {
    PDImgTypeNormal = 0,
    PDImgTypeAvatar = 1,
    PDImgTypeGift = 2,              /**< 礼物*/
    PDImgTypeRoot = 3,              /**< 原图*/
};

@interface PDImageView : UIImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
// 更新头像方法
-(void)uploadImageWithAvatarURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
-(void)uploadMainImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder imgType:(PDImgType)type callback:(void(^)(UIImage *image))callbackBlock;
-(void)uploadImageRootWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;


+(void)cleanImgCacheWithBlock:(void(^)())block;
+(NSString *)diskCount;

+(NSString *)appendingImgUrl:(NSString *)urlString;
@end
