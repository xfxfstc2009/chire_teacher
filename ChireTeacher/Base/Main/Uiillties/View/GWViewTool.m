//
//  GWViewTool.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWViewTool.h"

@implementation GWViewTool


+(UITableView *)gwCreateTableViewRect:(CGRect)rect{
    UITableView *tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.scrollEnabled = YES;
    return tableView;
}

+(UICollectionView *)gwCreateCollectionViewRect:(CGRect)rect{
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.showsVerticalScrollIndicator = NO;
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight| UIViewAutoresizingFlexibleWidth;
    return collectionView;
}

+(UIScrollView *)gwCreateScrollViewWithRect:(CGRect)rect{
    UIScrollView *mainScrollView = [[UIScrollView alloc]init];
    mainScrollView.frame = rect;
    mainScrollView.backgroundColor = [UIColor clearColor];
    mainScrollView.pagingEnabled = YES;
    mainScrollView.alwaysBounceHorizontal = NO;
    mainScrollView.alwaysBounceVertical = NO;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    mainScrollView.showsVerticalScrollIndicator = NO;
//    mainScrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    return mainScrollView;
}


+(UILabel *)createLabelFont:(NSString *)font textColor:(NSString *)color{
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithCustomerName:color];
    label.font = [UIFont fontWithCustomerSizeName:font];
    return label;
}

+(UIView *)createBottomButtonViewframe:(CGRect)frame image:(UIImage *)image label:(NSString *)text buttonBlock:(void(^)())block{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    view.frame = frame;
    
    // 2. 创建图片
    PDImageView *imageView = [[PDImageView alloc]init];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = image;
    [view addSubview:imageView];
    
    // 3. 创建label
    UILabel *titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    titleLabel.text = text;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    
    
    // 4. 创建按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = view.bounds;
    __weak typeof(self)weakSelf = self;
    [button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    [view addSubview:button];
    
    return view;
}


+(UIView *)goButtonWithTitle:(NSString *)title block:(void(^)())block{
    UIView *infoView = [[UIView alloc]init];
    infoView.backgroundColor = [UIColor colorWithCustomerName:@"棕"];
    
    UILabel *goLabel = [[UILabel alloc]init];
    goLabel.backgroundColor = [UIColor clearColor];
    goLabel.text = title;
    goLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [infoView addSubview:goLabel];
    
    CGSize goSize = [goLabel.text sizeWithCalcFont:goLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:goLabel.font])];
    goLabel.frame = CGRectMake(LCFloat(7), LCFloat(3), goSize.width, [NSString contentofHeightWithFont:goLabel.font]);
    
    PDImageView *arrowImgView = [[PDImageView alloc]init];
    arrowImgView.backgroundColor = [UIColor clearColor];
    arrowImgView.image = [UIImage imageNamed:@"sy_ic_jinru"];
    arrowImgView.frame = CGRectMake(CGRectGetMaxX(goLabel.frame) + LCFloat(2), 0, LCFloat(8), LCFloat(12));
    arrowImgView.center_y = goLabel.center_y;
    [infoView addSubview:arrowImgView];
    
    infoView.frame = CGRectMake(0, 0, goSize.width + LCFloat(2) + 2 * LCFloat(7) + LCFloat(8), 2 * LCFloat(3) + [NSString contentofHeightWithFont:goLabel.font]);
    infoView.clipsToBounds = YES;
    infoView.layer.cornerRadius = MIN(infoView.size_height, infoView.size_width) / 2.;
    return infoView;
}

#pragma mark - Segment
-(HTHorizontalSelectionList *)createSegment{
    HTHorizontalSelectionList *segmentList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44))];
    [segmentList setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    segmentList.selectionIndicatorColor = c15;
    segmentList.bottomTrimColor = [UIColor clearColor];
    segmentList.isNotScroll = YES;
    [segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    segmentList.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    return segmentList;
}

+(void)setTransferSingleAsset:(UIImage *)transferSingleAsset imgView:(PDImageView*)imgView convertView:(UIView *)convertView{
    CGImageRef posterImage = transferSingleAsset.CGImage;
    // 1. 获取当前图片高度
    size_t posterImageHeight = CGImageGetHeight(posterImage);
    CGFloat scale = posterImageHeight / convertView.size_height;
    imgView.image = [UIImage imageWithCGImage:posterImage scale:scale orientation:UIImageOrientationUp];
    
    // 基本尺寸参数
    CGSize boundsSize = convertView.bounds.size;
    CGFloat boundsWidth = boundsSize.width;
    CGFloat boundsHeight = boundsSize.height;
    
    CGSize imageSize = imgView.image.size;
    CGFloat imageWidth = imageSize.width;
    CGFloat imageHeight = imageSize.height;
    
    if (imageWidth == 0 || imageHeight == 0){
        return;
    }
    CGRect imageFrame = CGRectMake(0, (boundsHeight - boundsWidth) / 2., boundsWidth, imageHeight * boundsWidth / imageWidth);
    
    if (imageHeight > imageWidth){          // 图片高度大于图片宽度
        if (imageFrame.size.height < boundsHeight) {
            imageFrame.origin.y = floorf((boundsHeight - imageFrame.size.height) / 2.0);
        } else {
            imageFrame.origin.y = 0;
        }
    } else {                                // 图片宽度大于高度
        if (imageFrame.size.width < boundsWidth){
            imageFrame.origin.x = 0;
            imageFrame.size.width = (boundsHeight * 1.00 / imageFrame.size.height) *imageFrame.size.width;
            imageFrame.size.height = boundsHeight;
        } else {
            imageFrame.size.width = boundsWidth;
            imageFrame.size.height = (boundsWidth / imageFrame.size.width) * imageFrame.size.height;
            imageFrame.origin.x = - floorf((imageFrame.size.width - boundsWidth) / 2.0);
            imageFrame.origin.y = - floorf((imageFrame.size.height - boundsHeight) / 2.);
            
            if (imageFrame.size.height < boundsHeight){
                imageFrame.size.height = boundsHeight;
                imageFrame.size.width = (boundsHeight / imageFrame.size.height) * boundsWidth;
                imageFrame.origin.x = - floorf((imageFrame.size.width - boundsWidth) / 2.0);
                imageFrame.origin.y = - floorf((imageFrame.size.height - boundsHeight) / 2.);
            }
            
        }
    }
    imgView.frame = imageFrame;
}

-(UITextField *)inputTextField{
    //2.创建输入框
    UITextField *inputTextField = [[UITextField alloc]init];
    inputTextField.backgroundColor = [UIColor clearColor];
    inputTextField.placeholder = @"";
    inputTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    return inputTextField;
}

@end
