//
//  GWIconTableViewCell.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/6.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWIconTableViewCell : PDBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;                  /**< 传入的标题*/
@property (nonatomic,strong)UIImage *transferIcon;                  /**< 传入的icon*/
@property (nonatomic,copy)NSString *transferDesc;                   /**< 传入的内容*/
@property (nonatomic,assign)BOOL transferHasArrow;                  /**< 是否箭头*/
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

+(CGFloat)calculationCellHeight;

@end
