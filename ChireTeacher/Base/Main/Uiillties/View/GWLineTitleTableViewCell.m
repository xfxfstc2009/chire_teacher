//
//  GWLineTitleTableViewCell.m
//  ChireTeacher
//
//  Created by 裴烨烽 on 2019/2/1.
//  Copyright © 2019 炽热教育. All rights reserved.
//

#import "GWLineTitleTableViewCell.h"

@interface GWLineTitleTableViewCell()
@property (nonatomic,strong)UIView *itemView;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation GWLineTitleTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mrak - createView
-(void)createView{
    self.itemView = [[UIView alloc]init];
    self.itemView.backgroundColor = [UIColor redColor];
    [self addSubview:self.itemView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    
    self.itemView.frame = CGRectMake(LCFloat(12), ([GWLineTitleTableViewCell calculationCellHeight] - LCFloat(15)) / 2., LCFloat(3), LCFloat(15));
    
    self.titleLabel.text = transferTitle;
    self.titleLabel.font = [[UIFont systemFontOfCustomeSize:15.] boldFont];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.itemView.frame) + LCFloat(7), 0, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.itemView.frame) - LCFloat(7), [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.center_y = self.itemView.center_y;
}

+(CGFloat)calculationCellHeight{
    return 44;
}

@end
