//
//  NetworkAdapter.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//
// 网络适配器
#import <Foundation/Foundation.h>

#define PDBizErrorDomain @"PDBizErrorDomain"

@interface NetworkAdapter : NSObject

+(instancetype)sharedAdapter;                                                               /**< 单例*/

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(void(^)(BOOL isSucceeded,id responseObject, NSError *error))block;                                                          /**< 短链接*/

+ (NSString*)dictionaryToJson:(NSDictionary *)dic;



@end
