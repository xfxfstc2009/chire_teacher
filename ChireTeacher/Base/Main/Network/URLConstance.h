//
//  URLConstance.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#ifndef URLConstance_h
#define URLConstance_h

// 【接口环境】
#ifdef DEBUG        // 测试
#define JAVA_Host @"www.chire.net"
#define JAVA_Port @""
#else               // 线上
#define JAVA_Host @"www.chire.net"
#define JAVA_Port @""
#endif


// 【配置】
static NSString *const ErrorDomain = @"ErrorDomain";
static BOOL NetLogAlert = YES;                                                  /**< 网络输出*/
static NSInteger TimeoutInterval = 15;                                          /**< 短链接失效时间*/

// 【长链接】
static BOOL isHeartbeatPacket = NO;                                            /**< 是否心跳包*/
static BOOL isSocketLogAlert = NO;                                             /**< 是否socket*/

#ifdef DEBUG
#define PDSocketDebug
#endif

#ifdef PDSocketDebug
#define PDSocketLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define PDSocketLog(format, ...)
#endif

// 【Home】
static NSString *Class_List = @"ChireInter/Class/Class_List.ashx";
static NSString *Class_Add = @"ChireInter/Class/Class_Add.ashx";
static NSString *Class_Del = @"ChireInter/Class/Class_Del.ashx";

// 【箴言】
static NSString *Motto_List = @"ChireInter/Motto/Motto_List.ashx";
static NSString *Motto_Add = @"ChireInter/Motto/Motto_Add.ashx";
static NSString *Motto_Del = @"ChireInter/Motto/Motto_Del.ashx";

// 【学校】
static NSString *School_List = @"ChireInter/School/School_List.ashx";

// 【学生】
static NSString *Student_List = @"ChireInter/Students/Students_List.ashx";
static NSString *Student_Detail = @"ChireInter/Students/Students_Info.ashx";


// 【作业】
static NSString *Work_List = @"ChireInter/Work/Work_List.ashx";
static NSString *Work_Add = @"ChireInter/Work/Work_Add.ashx";

// 【微信通知】
static NSString *wechat_notice_list = @"Wechat/Wechat_Notice_List.ashx";

// 【考试资料列表】
static NSString *Exam_File_List = @"ChireInter/Exam/Exam_File_List.ashx";
static NSString *Exam_List = @"ChireInter/Exam/Exam_List.ashx";


// 【学习资料列表】
static NSString *Study_File_List = @"ChireInter/Download/Download_List.ashx";
static NSString *Study_File_Add = @"ChireInter/Download/Download_Add.ashx";

// 【签到】
static NSString *Sign_List = @"ChireInter/Sign/Sign_Class_List.ashx";
static NSString *Sign_Class_Detail = @"ChireInter/Sign/Sign_Class_Detail.ashx";

// 【学生列表】
static NSString *wechat_location_list = @"ChireInter/Wechat/Wechat_Location_List.ashx";

// 获取首页的图片
static NSString *banner_img_list = @"ChireInter/Window/View/Window_Img_List.ashx";

// 【老师列表】
static NSString *teacher_list = @"ChireInter/Teacher/Teacher_List.ashx";

// 【下载】
static NSString *download_Add = @"ChireInter/DownLoad/DownLoad_Add.ashx";

#endif /* URLConstance_h */
